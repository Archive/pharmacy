/* Thanks to Pavel Roskin for pointing out this
fix */

#undef PACKAGE
#undef VERSION
#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef HAVE_LC_MESSAGES
#undef HAVE_LIBSM
#undef HAVE_STPCPY
#undef HAVE_STRPTIME


