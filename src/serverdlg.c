/*  serverdlg.c
 *    This file is part of "Pharmacy: A GNOME CVS front-end"
 *    Copyright 1998 Reklaw (N. Adam Walker)
 *    Copyright 2000, 2001, 2002 Matthias Kranz
 *    "Pharmacy" is released under the GPL.
 *    See the LICENCE for more details.
 *    See AUTHORS for more details about contributed work.
 */

/* Includes */
#include <gnome.h>
#include <assert.h>

#include "pharmacy.h"
#include "location.h"

#include "serverdlg.h"

/* Functions */
void serverDlg_addServer (GtkButton*);
void serverDlg_removeServer (GtkButton*);
void serverDlg_serverSelected (GtkWidget*, gint, gint, GdkEventButton*, gpointer);
void serverDlg_serverUnselected (GtkWidget*, gint, gint, GdkEventButton*, gpointer);
void serverDlg_commitChanges (GtkButton*);
void serverDlg_discardChanges (GtkButton*);
void serverDlg_displayServerData (LocationData*);
void serverDlg_entryEdited (GtkEditable*);
GtkWidget *serverDlg_makeButtonInServerFrame (GtkWidget*, gchar*, void*);
void serverDlg_build (void);
gint serverDlg_addListItem (gpointer, gpointer);
void serverDlg_createList (GList*);
gint serverDlg_saveList (void);

/* Struct */
static GtkWidget *pgDlg = NULL;
static GtkWidget *pcList = NULL;
static GtkWidget *pFrameServerForm = NULL;

/*Buttons */
static GtkWidget *pbRemoveServer = NULL;
static GtkWidget *pbDiscardChanges = NULL;
static GtkWidget *pbCommitChanges = NULL;

/* Entry Boxes & Labels */
#define R_NAME 0
#define R_SERVER 1
#define R_WORK_DIR 2
#define R_NUM_ENTRIES 3
static GtkWidget *apEntries[] = {NULL, NULL, NULL};
static GtkWidget *apLabels[] = {NULL, NULL, NULL};
static gchar *labelNaming[] = {_ ("Name"), _ ("CVSROOT"), _ ("Working Dir")};

static gchar *titles[] = {_ ("Name")};

gint nListRowSelected = -1;

void
serverDlg_addServer (GtkButton * button)
{
	LocationData *pLoc = NULL;
	gint row;
	
	pLoc = locationdata_new_with_values (_ ("new server"), "", "");
	row = serverDlg_addListItem((gpointer) pLoc, NULL);

	gtk_clist_select_row (GTK_CLIST (pcList), row, 0);
}

void
serverDlg_removeServer (GtkButton * button)
{
	gint row2Remove;

	if (nListRowSelected > -1)
	{
		row2Remove = nListRowSelected;

		gtk_clist_remove (GTK_CLIST (pcList), row2Remove);

		/* select the server on the next row of the list or the last if no one is after */
		if(gtk_clist_get_row_data(GTK_CLIST (pcList), row2Remove) == 0)
			gtk_clist_select_row(GTK_CLIST (pcList), row2Remove - 1, 0);
		else
			gtk_clist_select_row(GTK_CLIST (pcList), row2Remove, 0);
	}
}

void
serverDlg_serverSelected (GtkWidget * widget, gint row, gint column,
		                    GdkEventButton * event, gpointer data)
{
	LocationData *pLoc;
  
	pLoc = (LocationData *) gtk_clist_get_row_data (GTK_CLIST (widget), row);

	nListRowSelected = row;

	/* Put data in the edit area */
	serverDlg_displayServerData(pLoc);

	gtk_widget_set_sensitive (pbCommitChanges, FALSE);
	gtk_widget_set_sensitive (pbDiscardChanges, FALSE);
	gtk_widget_set_sensitive (pbRemoveServer, TRUE);

	gtk_widget_show (GTK_WIDGET (pFrameServerForm));
}

void
serverDlg_serverUnselected (GtkWidget * widget, gint row, gint column,
		                      GdkEventButton * event, gpointer data)
{
	LocationData *pLoc;
	gchar *entry[R_NUM_ENTRIES];
	gboolean dataChanged;
	GtkWidget *pDialog;
	gint i;
	int result;
	
	pLoc = (LocationData *) gtk_clist_get_row_data (GTK_CLIST (pcList), row);

	if (pLoc)
	{
		entry[0] = (gchar*) locationdata_get_name (pLoc);
		entry[1] = (gchar*) locationdata_get_server (pLoc);
		entry[2] = (gchar*) locationdata_get_workdir (pLoc);
	} 
	else
	{
		entry[0] = NULL;
		entry[1] = NULL;
		entry[2] = NULL;
	}

	dataChanged = FALSE;		
	for(i = 0; i < R_NUM_ENTRIES; ++i)
	{
		if(entry[i] == NULL)
		{
			if(strcmp("", gtk_entry_get_text (GTK_ENTRY (apEntries[i]))) != 0)
				dataChanged = TRUE;
		}
		else
			if(strcmp(entry[i], gtk_entry_get_text (GTK_ENTRY (apEntries[i]))) != 0)
				dataChanged = TRUE;
	} 

	/* ask the user to save changed entries */
	if(dataChanged)
	{
		pDialog = gnome_message_box_new (
		           _ ("You changed the data of the server.\nDo you want to save the changes?"),
		           GNOME_MESSAGE_BOX_QUESTION,
		           GNOME_STOCK_BUTTON_YES,
		           GNOME_STOCK_BUTTON_NO,
		           NULL);
		gnome_dialog_set_default (GNOME_DIALOG (pDialog), 0);
		result = gnome_dialog_run_and_close (GNOME_DIALOG (pDialog));
		if(result == 0)  /* Yes */
			serverDlg_commitChanges(NULL);
	}
  
	nListRowSelected = -1;
	gtk_widget_hide (GTK_WIDGET (pFrameServerForm));
	gtk_widget_set_sensitive (pbRemoveServer, FALSE);
	gtk_widget_set_sensitive (pbDiscardChanges, FALSE);
}

void
serverDlg_commitChanges (GtkButton * button)
{
	LocationData *pLoc = NULL;
	gchar *serverName;

	if (nListRowSelected != -1)
	{
		pLoc = (LocationData *) gtk_clist_get_row_data (GTK_CLIST (pcList),
		                                                nListRowSelected);
		serverName = g_strdup (gtk_entry_get_text (GTK_ENTRY (apEntries[R_NAME])));

		locationdata_set_values (pLoc, serverName,
		                         gtk_entry_get_text (GTK_ENTRY (apEntries[R_SERVER])),
		                         gtk_entry_get_text (GTK_ENTRY (apEntries[R_WORK_DIR])));

		gtk_clist_set_text (GTK_CLIST (pcList), nListRowSelected, 0, serverName);
		gtk_clist_set_row_data (GTK_CLIST (pcList), nListRowSelected, pLoc);
      
		gtk_widget_set_sensitive (pbCommitChanges, FALSE);
		gtk_widget_set_sensitive (pbDiscardChanges, FALSE);
	}
}

void
serverDlg_discardChanges (GtkButton * button)
{
	LocationData *pLoc;

	pLoc = (LocationData *) gtk_clist_get_row_data (GTK_CLIST (pcList),
	                                                nListRowSelected);

	if (pLoc)
	{
		/* Put old data in the edit area */
		serverDlg_displayServerData(pLoc);
		
		/* disable the buttons */
		gtk_widget_set_sensitive (pbCommitChanges, FALSE);
		gtk_widget_set_sensitive (pbDiscardChanges, FALSE);
	} 
}

void
serverDlg_displayServerData (LocationData* pServerData)
{
	gint i;
	gchar *entry[R_NUM_ENTRIES];

	if (pServerData)
	{
		entry[0] = (gchar*) locationdata_get_name (pServerData);
		entry[1] = (gchar*) locationdata_get_server (pServerData);
		entry[2] = (gchar*) locationdata_get_workdir (pServerData);
	} 
	else
	{
		entry[0] = NULL;
		entry[1] = NULL;
		entry[2] = NULL;
	} 
		
	for(i = 0; i < R_NUM_ENTRIES; ++i)
	{
		if(entry[i])
			gtk_entry_set_text (GTK_ENTRY (apEntries[i]), entry[i]);
		else
			gtk_entry_set_text (GTK_ENTRY (apEntries[i]), "");
	} 
}

void
serverDlg_entryEdited (GtkEditable *editable)
{
	gtk_widget_set_sensitive (pbCommitChanges, TRUE);
	gtk_widget_set_sensitive (pbDiscardChanges, TRUE);
}

GtkWidget *
serverDlg_makeButtonInServerFrame (GtkWidget * pBox, gchar * pgcChar,
			                          void *pFunc)
{
	GtkWidget *pWidget;

	pWidget = gtk_button_new_with_label (pgcChar);
	gtk_box_pack_start (GTK_BOX (pBox), pWidget, FALSE, FALSE, 0);
	gtk_widget_show (pWidget);

	if (pFunc)
		gtk_signal_connect (GTK_OBJECT (pWidget), _ ("clicked"),
		                    GTK_SIGNAL_FUNC (pFunc), NULL);

	gtk_widget_set_sensitive (pWidget, FALSE);
	
	return pWidget;
}

/* Build The Servers Dialog */
void
serverDlg_build (void)
{
	const int rowSpacingTableDetails = 5;
	int i;
	GtkWidget *phBox = NULL;
	GtkWidget *pvListBox = NULL;
	GtkWidget *pbAddServer = NULL;
	GtkWidget *pButtonBox = NULL;
	GtkWidget *pServerForm = NULL;
	GtkWidget *pTableDetails = NULL;
	GtkWidget *pFileEntryWorkingDir = NULL;

	pgDlg = gnome_dialog_new (_ ("Servers"), GNOME_STOCK_BUTTON_CLOSE, NULL);
	gtk_signal_connect (GTK_OBJECT (pgDlg), "close", (GFunc) serverDlg_saveList, NULL);

	gtk_widget_set_usize (pgDlg, 640, 400);

	phBox = gtk_hbox_new (0, 0);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (pgDlg)->vbox), phBox, TRUE, TRUE, 0);
  
	pvListBox = gtk_vbox_new (0, 0);
	gtk_box_pack_start (GTK_BOX (phBox), pvListBox, TRUE, TRUE, 5);

	pcList = gtk_clist_new_with_titles (1, titles);
	gtk_box_pack_start (GTK_BOX (pvListBox), util_contain_in_scrolled (pcList), TRUE, TRUE, 0);
	gtk_signal_connect (GTK_OBJECT (pcList), "select_row", serverDlg_serverSelected, NULL);
	gtk_signal_connect (GTK_OBJECT (pcList), "unselect_row", serverDlg_serverUnselected, NULL);

	/* Button "Add Server" under List */
	pbAddServer = gtk_button_new_with_label ( _ ("Add Server"));
	gtk_box_pack_start (GTK_BOX (pvListBox), pbAddServer, FALSE, FALSE, 2);
	gtk_widget_show (pbAddServer);
	gtk_signal_connect (GTK_OBJECT (pbAddServer), _ ("clicked"),
	                    GTK_SIGNAL_FUNC (serverDlg_addServer), NULL);

	/* Server Form */
	pFrameServerForm = gtk_frame_new(_ ("Details on the server"));
	gtk_box_pack_start (GTK_BOX (phBox), pFrameServerForm, FALSE, FALSE, 5);
  
	pServerForm = gtk_vbox_new (0, 0);
	gtk_container_add (GTK_CONTAINER (pFrameServerForm), pServerForm);
  
	pTableDetails = gtk_table_new(3, 2, FALSE);
	gtk_table_set_row_spacings(GTK_TABLE (pTableDetails), rowSpacingTableDetails);
	gtk_box_pack_start (GTK_BOX (pServerForm), pTableDetails, FALSE, FALSE, 5);

	for(i = 0; i < 3; ++i)
	{
		apLabels[i] = gtk_label_new (labelNaming[i]);
		gtk_label_set_justify(GTK_LABEL (apLabels[i]), GTK_JUSTIFY_LEFT);
		gtk_table_attach_defaults (GTK_TABLE (pTableDetails), apLabels[i], 0, 1, i, i + 1);

	}
    
	apEntries[0] = gtk_entry_new_with_max_length (255);
	gtk_table_attach_defaults (GTK_TABLE (pTableDetails), apEntries[0], 1, 2, 0, 1);

	apEntries[1] = gtk_entry_new_with_max_length (255);
	gtk_table_attach_defaults (GTK_TABLE (pTableDetails), apEntries[1], 1, 2, 1, 2);

	pFileEntryWorkingDir = gnome_file_entry_new ("/", _ ("Select work directory"));
    gnome_file_entry_set_directory(GNOME_FILE_ENTRY (pFileEntryWorkingDir), TRUE);
	gtk_table_attach_defaults (GTK_TABLE (pTableDetails), pFileEntryWorkingDir, 1, 2, 2, 3);
	
	apEntries[2] = gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (pFileEntryWorkingDir));
	
	for(i = 0; i < 3; ++i)
		gtk_signal_connect (GTK_OBJECT (apEntries[i]), "changed", serverDlg_entryEdited, NULL);
	
	/* Buttons on Server Form*/
	pButtonBox = gtk_hbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (pServerForm), pButtonBox, FALSE, FALSE, 5);
  
	pbCommitChanges  = serverDlg_makeButtonInServerFrame (pButtonBox, _ ("Commit Changes"),
	                                                      serverDlg_commitChanges);
	pbDiscardChanges = serverDlg_makeButtonInServerFrame (pButtonBox, _ ("Discard Changes"),
	                                                      serverDlg_discardChanges);
	pbRemoveServer   = serverDlg_makeButtonInServerFrame (pButtonBox, _ ("Remove Server"),
	                                                      serverDlg_removeServer);  
  
	gtk_widget_show_all (phBox);
	gtk_widget_hide (pFrameServerForm);  /* todo: select one entry */
}

gint
serverDlg_addListItem (gpointer pData, gpointer pDontCare)
{
	gchar *entry[] = {NULL, NULL, NULL};
	gint row;
	
	if (pData)
	{
		entry[0] = g_strdup (locationdata_get_name (pData));

		row = gtk_clist_append (GTK_CLIST (pcList), entry);
		gtk_clist_set_row_data (GTK_CLIST (pcList), row, pData);
	}
	else
		row = -1;
	
	return (row);
}

void
serverDlg_createList (GList *pList)
{
	LocationData *pLocation;
	
	assert (pList != NULL);
	
	/* i don't know why, but i need the next line to get a correct behaviour */
	pList = g_list_next (pList);
	
	while (pList != NULL)
	{
		pLocation = locationdata_duplicate ((LocationData *) pList->data);
		serverDlg_addListItem (pLocation, NULL);
		
		pList = g_list_next (pList);
	}
}

gint
serverDlg_saveList (void)
{
	GList* pServerListStart;
	GList* pServerListElement;
	gpointer pLocation;
	gint row;
	
	pServerListStart = pharmacy_get_locations ();

	/* delete all entries from the old list */
	pServerListElement = pServerListStart;
	
	while(pServerListElement)
	{
		locationdata_free (pServerListElement->data);
		pServerListElement = g_list_next (pServerListElement);
	}

	g_list_free (pServerListStart);
	pServerListStart = g_list_alloc();

	/* insert entries of the clist */
	row = 0;
	while (pLocation = gtk_clist_get_row_data (GTK_CLIST (pcList), row))
	{ 
		g_list_append (pServerListStart, pLocation);
		++row;
	}
	
	pharmacy_set_locations (pServerListStart);

	/* save list */
	locations_save_list (pharmacy_get_locations ());
	
	return (FALSE);
}

/**
 * Menu item callback.
 * Displays the Server Dialog.
 */
void
serverDlg_cb (GtkWidget * widget, void *data)
{
	serverDlg_build ();

	assert (pgDlg != NULL);
	
	serverDlg_createList (pharmacy_get_locations ());
	gnome_dialog_run_and_close (GNOME_DIALOG (pgDlg));

	/* Dialog was closed */
	pharmacy_rebuild_entire_dirtree ();
}
