
#ifndef R_LIST_H
#define R_LIST_H 1

#include "dirtree.h"

typedef struct _FileListNode FileListNode;
typedef enum _FileListNodeStatus FileListNodeStatus;

enum _FileListNodeStatus
  {
    FLN_STATUS_UNKNOWN,
    FLN_STATUS_NOT_IN_CVS,
    FLN_STATUS_IGNORE,
    FLN_STATUS_NORMAL,
    FLN_STATUS_NEEDS_PATCH,
    FLN_STATUS_ADDED,
    FLN_STATUS_MODIFIED,
    FLN_STATUS_REMOVED,
    FLN_STATUS_NEEDS_CHECKOUT,
    FLN_STATUS_NEEDS_MERGE
  };

#define CVS_FILE 1
#define CVS_DIR 2

struct _FileListNode
  {
    gint type;
    gchar *pgcName;
    gchar *pgcPath;
    gchar *pgcVersion;
    DirTreeNode *pDirTreeNode;
    FileListNodeStatus eStatus;
  };

#define FILE_LIST(obj) GTK_CHECK_CAST(obj,filelist_get_type(), FileList)
#define FILE_LIST_CLASS(klass) GTK_CHECK_CAST_CLASS(klass, filelist_get_type())
#define IS_FILE_LIST(obj) GTK_CHECK_TYPE(obj, filelist_get_type())

typedef struct _FileList FileList;
typedef struct _FileListClass FileListClass;

struct _FileList
  {
    GtkCList parent_object;

    /* Images */
    GdkPixmap *pmQuestion;
    GdkBitmap *mkQuestion;

    GdkPixmap *pmAdded;
    GdkBitmap *mkAdded;

    GdkPixmap *pmModified;
    GdkBitmap *mkModified;

    GdkPixmap *pmRemoved;
    GdkBitmap *mkRemoved;

    GdkPixmap *pmNeedsPatch;
    GdkBitmap *mkNeedsPatch;

    GdkPixmap *pmNeedsCheckout;
    GdkBitmap *mkNeedsCheckout;

    GdkPixmap *pmNeedsMerge;
    GdkBitmap *mkNeedsMerge;

    GdkPixmap *pmIgnore;
    GdkBitmap *mkIgnore;

    GdkPixmap *pmNotInCvs;
    GdkBitmap *mkNotInCvs;

    gint nName;
    gint nVersion;
    gint nStatus;
    gint nDate;
    gint nCols;

    GList *entries;
  };

struct _FileListClass
  {
    GtkCListClass parent_class;
  };


guint filelist_get_type ();
GtkWidget *filelist_new ();

FileListNode *filelistnode_new ();
void filelist_build_list (FileList * pList, DirTreeNode * pTreeNode);
void filelist_rebuild_list (FileList * pList, DirTreeNode * pTreeNode);
void filelistnode_free (FileListNode * pNode);
GtkWidget *filelist_new_with_titles (const int count, gchar ** titles);

void filelist_load_size (GtkWidget * pWidget);
void filelist_save_size (GtkWidget * pWidget);
gchar *filelist_get_selected_files (FileList * pList);
gint filelist_find_row_with_filename (FileList * pList, const gchar * pgcTarget);
void filelist_row_set_status (FileList * pList, gint row, FileListNodeStatus eStatus);

void filelist_fill_entries (FileList * pList, gchar * path);
GList *filelist_get_dir_entries (FileList * pList);
void filelist_cleanse (FileList * pList);
void filelist_set_row_text(FileList* pList, const gint nRow,
		      gchar** text);

#endif /* R_LIST_H */
