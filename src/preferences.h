#ifndef R_PREFERENCES_H
#define R_PREFERENCES_H 1
   void preferences_cb();
   void preferences_ui_update_cb();
void load_prefs();
gchar * cvs_checkout_get_options();
gchar * cvs_commit_get_options();
gchar * cvs_global_get_options();
gchar * cvs_status_get_options();
gchar * cvs_update_get_options();



typedef struct _PharmacyConfigDlg PharmacyConfigDlg;
typedef struct _PharmacyConfig PharmacyConfig;


struct _PharmacyConfigDlg
{
  GnomeDialog* dialog;
};

struct _PharmacyConfig
  {
    gboolean read_cvsrc;
    gboolean fakewrite;
    gboolean logcommand;
    gboolean readonly;
    gboolean bEncrypt;
    gchar *bindir;
    gchar *editor;
    gchar *tmpdir;
    gint compression_level;

    gboolean checkout_shortenpath;
    gboolean checkout_pruneempty;
    gboolean checkout_localdir;
    gboolean checkout_runprog;
    gboolean checkout_forcerevision;

    gboolean status_localdir;

    gboolean update_createdirs;
    gboolean update_pruneempty;
    gboolean update_localdir;
    gboolean update_runprog;
    gboolean update_forcerevision;

    gboolean commit_localdir;
    gboolean commit_runprog;

    gboolean debug_debug;
    gboolean debug_show_test_stuff;
  };

PharmacyConfigDlg *pharmacy_config_dlg_new ();


/* 
   Use these functions if you want to create a dialog with the same
   widgets as the notebook page in the config dlg.
 */
void config_global_build_ui(GtkVBox* vbox);
void config_checkout_build_ui(GtkVBox* vbox);
void config_status_build_ui(GtkVBox* vbox);
void config_update_build_ui(GtkVBox* vbox);
void config_commit_build_ui(GtkVBox* vbox);
void config_debug_build_ui(GtkVBox* vbox);

#endif

