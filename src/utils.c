#include<gnome.h>

#define g_list_get_data(a) g_list_nth_data(a,0)

GtkWidget *
util_contain_in_scrolled (GtkWidget * pWidget)
{
  GtkWidget *pTemp = NULL;
  pTemp = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (pTemp),
				  //GTK_POLICY_AUTOMATIC,
				  //GTK_POLICY_AUTOMATIC);
				  GTK_POLICY_ALWAYS,
				  GTK_POLICY_ALWAYS);
  gtk_container_add (GTK_CONTAINER (pTemp), pWidget);
  gtk_widget_show (pTemp);
  return pTemp;
}


gint g_bDebug = FALSE;
gint g_bExper = FALSE;

void
g_debug_message(const gchar* format, ...)
{
  va_list va;


  if(g_bDebug)
    {
      va_start(va,format);
      g_logv(G_LOG_DOMAIN,
	     G_LOG_LEVEL_MESSAGE,
	     format,
	     /*va_list         args*/va);
      va_end(va);
    }

}
