#ifndef R_QUE_H
#define R_QUE_H 1

#include "dirtree.h"
#include "filelist.h"

   typedef struct _QueNode QueNode;
   typedef enum {Type_None, Type_File, Type_Dir} QueNodeType;
   typedef enum {Action_None, Action_Update,
   Action_Commit, Action_Checkout }
   QueNodeAction;

   struct _QueNode
   {
      QueNodeType eType;
      QueNodeAction eAction;
      gint bFinished;
      FileListNode* pFile;
      DirTreeNode* pDir;
   };

   QueNode* quenode_new();

#endif /* R_QUE_H */
