#ifndef PHARMACY_STOCK_H
#define PHARMACY_STOCK_H 1

#define PHARMACY_STOCK_PIXMAP_CHECKOUT "Pharmacy_Checkout"
#define PHARMACY_STOCK_PIXMAP_BOLT "Pharmacy_Bolt"
#define PHARMACY_STOCK_PIXMAP_FISH "Pharmacy_Fish"
#define PHARMACY_STOCK_PIXMAP_CVSDIR "Pharmacy_CVSdir"

void
pharmacy_stock_init();


#endif /* PHARMACY_STOCK_H */
