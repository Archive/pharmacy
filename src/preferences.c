/* preferences.c
 * This file is part of "Pharmacy: A GNOME CVS front-end"
 * Copyright 1999 N. Adam Walker
 * Copyright 2000, 2001 Matthias Kranz
 * "Pharmacy" is released under the GPL.
 * See the LICENCE for more details.
 */

#include <config.h>
#include <gnome.h>

#include "preferences.h"
#include "pharmacy.h"
#include "location.h"

static gint aSignals[1];
static gint bPref = FALSE;

#define MENU 3
/*#define TOOLBAR 2 */

#define STRING_PROTECT(s) (s?s:"")

/* globals */
//static PharmacyConfigDlg *prefs_window = NULL;
static GtkWidget *cb_read_cvsrc;
static GtkWidget *cb_fakewrite;
static GtkWidget *cb_logcommand;
static GtkWidget *cb_readonly;
static GtkWidget *cb_bEncrypt;
static GtkWidget *e_bindir;
static GtkWidget *e_editor;
static GtkWidget *e_tmpdir;
static GtkWidget *compression_level;
static GtkWidget *cb_checkout_shortenpath;
static GtkWidget *cb_checkout_pruneempty;
static GtkWidget *cb_checkout_localdir;
static GtkWidget *cb_checkout_runprog;
static GtkWidget *cb_checkout_forcerevision;
static GtkWidget *cb_status_localdir;
static GtkWidget *cb_update_createdirs;
static GtkWidget *cb_update_pruneempty;
static GtkWidget *cb_update_localdir;
static GtkWidget *cb_update_runprog;
static GtkWidget *cb_update_forcerevision;
static GtkWidget *cb_commit_localdir;
static GtkWidget *cb_commit_runprog;
static GtkWidget *cb_debug_debug;
static GtkWidget *cb_debug_show_test_stuff;

PharmacyConfig *cfg = NULL;

static void
prefs_window_destroy (GtkWidget * widget, gpointer data)
{
    //gtk_widget_destroy (prefs_window);
    //prefs_window = NULL;

}

#define CONFIG_CHECKOUT_SHORTENPATH "/pharmacy/cvs-checkout/ShortenPath=true"
#define CONFIG_CHECKOUT_PRUNEEMPTY "/pharmacy/cvs-checkout/PruneEmpty=false"
#define CONFIG_CHECKOUT_LOCALDIR "/pharmacy/cvs-checkout/LocalDir=false"
#define CONFIG_CHECKOUT_RUNPROG "/pharmacy/cvs-checkout/RunProg=true"
#define CONFIG_CHECKOUT_FORCEREVISION "/pharmacy/cvs-checkout/ForceRevision=false"
#define CONFIG_COMMIT_LOCALDIR "/pharmacy/cvs-checkout/LocalDir=false"
#define CONFIG_COMMIT_RUNPROG "/pharmacy/cvs-checkout/RunProg=true"
#define CONFIG_READ_CVSRC "/pharmacy/cvs/Read_cvsrc=true"
#define CONFIG_FAKEWRITE "/pharmacy/cvs/fakeWrite=false"
#define CONFIG_LOGCOMMAND "/pharmacy/cvs/LogCommand=true"
#define CONFIG_READONLY "/pharmacy/cvs/ReadOnly=false"
#define CONFIG_ENCRYPT "/pharmacy/cvs/Encrypt=false"
#define CONFIG_BINDIR "/pharmacy/cvs/BinDir="
#define CONFIG_EDITOR "/pharmacy/cvs/Editor="
#define CONFIG_TMPDIR "/pharmacy/cvs/TmpDir="
#define CONFIG_COMPRESSION "/pharmacy/cvs/Compression=3"
#define CONFIG_STATUS_LOCALDIR "/pharmacy/cvs-status/LocalDir=false"
#define CONFIG_UPDATE_CREATEDIRS "/pharmacy/cvs-update/CreateDirs=false"
#define CONFIG_UPDATE_PRUNEEMPTY "/pharmacy/cvs-update/PruneEmpty=false"
#define CONFIG_UPDATE_LOCALDIR "/pharmacy/cvs-update/LocalDir=false"
#define CONFIG_UPDATE_RUNPROG "/pharmacy/cvs-update/RunProg=true"
#define CONFIG_UPDATE_FORCEREVISION "/pharmacy/cvs-update/ForceRevision=false"
#define CONFIG_DEBUG_DEBUG "/pharmacy/debug/debug=false"
#define CONFIG_DEBUG_SHOW_TEST_STUFF "/pharmacy/debug/ShowTestStuff=false"

static glong
save_prefs ()
{
    g_return_if_fail (cfg != NULL);
//co
    gnome_config_set_bool (CONFIG_CHECKOUT_SHORTENPATH,
                           cfg->checkout_shortenpath);
    gnome_config_set_bool (CONFIG_CHECKOUT_PRUNEEMPTY,
                           cfg->checkout_pruneempty);
    gnome_config_set_bool (CONFIG_CHECKOUT_LOCALDIR, cfg->checkout_localdir);
    gnome_config_set_bool (CONFIG_CHECKOUT_RUNPROG, cfg->checkout_runprog);
    gnome_config_set_bool (CONFIG_CHECKOUT_FORCEREVISION,
                           cfg->checkout_forcerevision);
//ci
    gnome_config_set_bool (CONFIG_COMMIT_LOCALDIR, cfg->commit_localdir);
    gnome_config_set_bool (CONFIG_COMMIT_RUNPROG, cfg->commit_runprog);
//global
    gnome_config_set_bool (CONFIG_READ_CVSRC, cfg->read_cvsrc);
    gnome_config_set_bool (CONFIG_FAKEWRITE, cfg->fakewrite);
    gnome_config_set_bool (CONFIG_LOGCOMMAND, cfg->logcommand);
    gnome_config_set_bool (CONFIG_READONLY, cfg->readonly);
    gnome_config_set_bool (CONFIG_ENCRYPT, cfg->bEncrypt);

    gnome_config_set_string (CONFIG_BINDIR, cfg->bindir);
    gnome_config_set_string (CONFIG_EDITOR, cfg->editor);
    gnome_config_set_string (CONFIG_TMPDIR, cfg->tmpdir);

    gnome_config_set_int (CONFIG_COMPRESSION, cfg->compression_level);
//status
    gnome_config_set_bool (CONFIG_STATUS_LOCALDIR, cfg->status_localdir);
//update
    gnome_config_set_bool (CONFIG_UPDATE_CREATEDIRS, cfg->update_createdirs);
    gnome_config_set_bool (CONFIG_UPDATE_PRUNEEMPTY, cfg->update_pruneempty);
    gnome_config_set_bool (CONFIG_UPDATE_LOCALDIR, cfg->update_localdir);
    gnome_config_set_bool (CONFIG_UPDATE_RUNPROG, cfg->update_runprog);
    gnome_config_set_bool (CONFIG_UPDATE_FORCEREVISION,
                           cfg->update_forcerevision);
    gnome_config_set_bool (CONFIG_DEBUG_DEBUG, cfg->debug_debug);
    gnome_config_set_bool (CONFIG_DEBUG_SHOW_TEST_STUFF,
                           cfg->debug_show_test_stuff);

    return 1;
}

void
load_prefs ()
{
    if (!cfg)
        cfg = g_malloc (sizeof (PharmacyConfig));
//co
    cfg->checkout_shortenpath =
        gnome_config_get_bool (CONFIG_CHECKOUT_SHORTENPATH);
    cfg->checkout_pruneempty =
        gnome_config_get_bool (CONFIG_CHECKOUT_PRUNEEMPTY);
    cfg->checkout_localdir = gnome_config_get_bool (CONFIG_CHECKOUT_LOCALDIR);
    cfg->checkout_runprog = gnome_config_get_bool (CONFIG_CHECKOUT_RUNPROG);
    cfg->checkout_forcerevision =
        gnome_config_get_bool (CONFIG_CHECKOUT_FORCEREVISION);
//ci
    cfg->commit_localdir = gnome_config_get_bool (CONFIG_COMMIT_LOCALDIR);
    cfg->commit_runprog = gnome_config_get_bool (CONFIG_COMMIT_RUNPROG);
//global
    cfg->read_cvsrc = gnome_config_get_bool (CONFIG_READ_CVSRC);
    cfg->fakewrite = gnome_config_get_bool (CONFIG_FAKEWRITE);
    cfg->logcommand = gnome_config_get_bool (CONFIG_LOGCOMMAND);
    cfg->readonly = gnome_config_get_bool (CONFIG_READONLY);
    cfg->bEncrypt = gnome_config_get_bool (CONFIG_ENCRYPT);

    cfg->bindir = g_strdup (gnome_config_get_string (CONFIG_BINDIR));
    cfg->editor = g_strdup (gnome_config_get_string (CONFIG_EDITOR));
    cfg->tmpdir = g_strdup (gnome_config_get_string (CONFIG_TMPDIR));

    cfg->compression_level = gnome_config_get_int (CONFIG_COMPRESSION);
//status
    cfg->status_localdir = gnome_config_get_bool (CONFIG_STATUS_LOCALDIR);
//update
    cfg->update_createdirs = gnome_config_get_bool (CONFIG_UPDATE_CREATEDIRS);
    cfg->update_pruneempty = gnome_config_get_bool (CONFIG_UPDATE_PRUNEEMPTY);
    cfg->update_localdir = gnome_config_get_bool (CONFIG_UPDATE_LOCALDIR);
    cfg->update_runprog = gnome_config_get_bool (CONFIG_UPDATE_RUNPROG);
    cfg->update_forcerevision =
        gnome_config_get_bool (CONFIG_UPDATE_FORCEREVISION);
    cfg->debug_debug = gnome_config_get_bool (CONFIG_DEBUG_DEBUG);
    g_bDebug = cfg->debug_debug;
    cfg->debug_show_test_stuff =
        gnome_config_get_bool (CONFIG_DEBUG_SHOW_TEST_STUFF);
    g_bExper = cfg->debug_show_test_stuff;;

    pharmacy_update_ui ("config");
}

  /////////////////////////////** MACRO
#define GUI_CHECKBOX_OPTION(o)  if (GTK_TOGGLE_BUTTON (cb_##o)->active)\
    cfg-> o = TRUE; else cfg-> o  = FALSE;
  //////////////////////////;

static void
prefs_ok_select ()
{
    gchar *ptr;

    GUI_CHECKBOX_OPTION (read_cvsrc);
    GUI_CHECKBOX_OPTION (fakewrite);
    GUI_CHECKBOX_OPTION (logcommand);
    GUI_CHECKBOX_OPTION (readonly);
    GUI_CHECKBOX_OPTION (bEncrypt);

    /////////////////////////////** MACRO
#define GUI_STRING_OPTION(o)  ptr = gtk_entry_get_text (GTK_ENTRY (e_##o));\
  if (ptr) {g_free(cfg-> o);cfg-> o = g_strdup(ptr);}
    //////////////////////////;

    GUI_STRING_OPTION (bindir);
    GUI_STRING_OPTION (editor);
    GUI_STRING_OPTION (tmpdir);

    {
        GtkAdjustment *adjustment;
        adjustment = gtk_range_get_adjustment (GTK_RANGE (compression_level));
        cfg->compression_level = adjustment->value;
    }

    GUI_CHECKBOX_OPTION (checkout_shortenpath);
    GUI_CHECKBOX_OPTION (checkout_localdir);
    GUI_CHECKBOX_OPTION (checkout_runprog);
    GUI_CHECKBOX_OPTION (checkout_forcerevision);
    GUI_CHECKBOX_OPTION (status_localdir);
    GUI_CHECKBOX_OPTION (update_createdirs);
    GUI_CHECKBOX_OPTION (update_pruneempty);
    GUI_CHECKBOX_OPTION (update_localdir);
    GUI_CHECKBOX_OPTION (update_runprog);
    GUI_CHECKBOX_OPTION (update_forcerevision);
    GUI_CHECKBOX_OPTION (commit_localdir);
    GUI_CHECKBOX_OPTION (commit_runprog);
    GUI_CHECKBOX_OPTION (debug_debug);
    g_bDebug = cfg->debug_debug;
    GUI_CHECKBOX_OPTION (debug_show_test_stuff);
    g_bExper = cfg->debug_show_test_stuff;;
}

void
preferences_ui_update_cb ()
{
    CVSConsole *pCon = pharmacy_get_console ();
    if (1)                      /*pCon && pCon->pCurrentNode && pCon->pCurrentNode->pData) */
        bPref = TRUE;
    else
        bPref = FALSE;

    gtk_widget_set_sensitive (tools_menu[MENU].widget, bPref);
}

#define CONFIG_BUTTON_OK 0
#define CONFIG_BUTTON_CANCEL 1

static void
config_dialog_button_clicked (GtkWidget * pWidget, gint nButton,
                              gpointer pData)
{
    PharmacyConfigDlg *dlg = (PharmacyConfigDlg *) pData;

    switch (nButton) {
    case CONFIG_BUTTON_OK:
        prefs_ok_select ();
        save_prefs ();
        gnome_dialog_close (dlg->dialog);
        break;
    case CONFIG_BUTTON_CANCEL:
        gnome_dialog_close (dlg->dialog);
        break;
    }

    pharmacy_update_ui ("config");
}

void
config_global_build_ui (GtkVBox * vbox)
{
    GtkWidget *hbox = NULL;
    GtkWidget *label;
    GtkObject *adjustment;

    /*      -f      Do not read the cvs startup file (~/.cvsrc). */
    cb_read_cvsrc =
        gtk_check_button_new_with_label (_
                                         ("Read the cvs startup file (~/.cvsrc"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_read_cvsrc, FALSE, TRUE, 5);
    gtk_widget_show (cb_read_cvsrc);
    /*   -n      Do not change any files.  Attempt  to  execute  the
       cvs_command,  but  only  to  issue  reports; do not
       remove, update, or merge  any  existing  files,  or
       create any new files. 
     */
    cb_fakewrite =
        gtk_check_button_new_with_label (_("Do not change any files"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_fakewrite, FALSE, TRUE, 5);
    gtk_widget_show (cb_fakewrite);
    /*   -l      Do  not  log the cvs_command in the command history
       (but execute it anyway).  See  the  description  of
       the history command for information on command his-
       tory.
     */
    cb_logcommand =
        gtk_check_button_new_with_label (_("Log the command in history"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_logcommand, FALSE, TRUE, 5);
    gtk_widget_show (cb_logcommand);
    /*   -r      Makes new working files read-only.  Same effect  as
       if the CVSREAD environment variable is set.
 *//* -w      Makes  new  working  files  read-write   (default).
   Overrides  the  setting  of the CVSREAD environment
   variable.
 */
    cb_readonly =
        gtk_check_button_new_with_label (_
                                         ("Make new working files read-only"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_readonly, FALSE, TRUE, 5);
    gtk_widget_show (cb_readonly);

    /*   -x      Encrypt all communication between  the  client  and
       the  server.   As  of  this  writing,  this is only
       implemented when using a Kerberos connection.
     */
    cb_bEncrypt =
        gtk_check_button_new_with_label (_("Encrypt all communication"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_bEncrypt, FALSE, TRUE, 5);
    gtk_widget_show (cb_bEncrypt);

    /*   -b bindir
       Use bindir as the directory where RCS programs  are
       located.  Overrides the setting of the RCSBIN envi-
       ronment variable.  This value should  be  specified
       as an absolute pathname.
     */
    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 10);
    gtk_widget_show (hbox);

    label = gtk_label_new (_("RCS programs path:"));
    gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 5);
    gtk_widget_show (label);

    e_bindir = gtk_entry_new_with_max_length (255);
    gtk_box_pack_start (GTK_BOX (hbox), e_bindir, TRUE, TRUE, 5);
    gtk_widget_show (e_bindir);

    /*   -e editor
       Use  editor  to  enter  revision  log  information.
       Overrides the setting of the CVSEDITOR and the EDI-
       TOR environment variables.
     */
    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 10);
    gtk_widget_show (hbox);

    label = gtk_label_new (_("Editor"));
    gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 5);
    gtk_widget_show (label);

    e_editor = gtk_entry_new_with_max_length (255);
    gtk_box_pack_start (GTK_BOX (hbox), e_editor, TRUE, TRUE, 5);
    gtk_widget_show (e_editor);

    /*   -T tmpdir
       Place for temporary files.
     */
    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 10);
    gtk_widget_show (hbox);

    label = gtk_label_new (_("Temporary directory"));
    gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 5);
    gtk_widget_show (label);

    e_tmpdir = gtk_entry_new_with_max_length (255);
    gtk_box_pack_start (GTK_BOX (hbox), e_tmpdir, TRUE, TRUE, 5);
    gtk_widget_show (e_tmpdir);

    /*   -z compression-level
       When transferring files across the network use gzip
       with  compression  level  compression-level to com-
       press and de-compress data as  it  is  transferred.
       Requires  the  presence  of the GNU gzip program in
       the current search path at both ends of the link.
     */
    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 10);
    gtk_widget_show (hbox);

    label = gtk_label_new (_("Compression level"));
    gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, TRUE, 5);
    gtk_widget_show (label);

    adjustment = gtk_adjustment_new (cfg->compression_level, 0, 9, 1, 1, 0);
    compression_level = gtk_hscale_new (GTK_ADJUSTMENT (adjustment));
    gtk_scale_set_digits (GTK_SCALE (compression_level), 0);
    gtk_box_pack_start (GTK_BOX (hbox), compression_level, TRUE, TRUE, 5);
    gtk_widget_show (compression_level);
}

void
config_checkout_build_ui (GtkVBox * vbox)
{
/*        -N      Don't shorten module paths if -d specified. */
    cb_checkout_shortenpath =
        gtk_check_button_new_with_label (_("Shorten module paths"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_checkout_shortenpath, FALSE, TRUE,
                        5);
    gtk_widget_show (cb_checkout_shortenpath);
/*        -P      Prune empty directories. */
    cb_checkout_pruneempty =
        gtk_check_button_new_with_label (_("Prune empty directories"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_checkout_pruneempty, FALSE, TRUE,
                        5);
    gtk_widget_show (cb_checkout_pruneempty);
/*        -l      Local directory only, not recursive */
    cb_checkout_localdir =
        gtk_check_button_new_with_label (_("Local directory only"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_checkout_localdir, FALSE, TRUE, 5);
    gtk_widget_show (cb_checkout_localdir);
/*        -n      Do not run module program (if any). */
    cb_checkout_runprog =
        gtk_check_button_new_with_label (_("Run module program"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_checkout_runprog, FALSE, TRUE, 5);
    gtk_widget_show (cb_checkout_runprog);
/*        -f      Force a head revision match if tag/date not found. */
    cb_checkout_forcerevision =
        gtk_check_button_new_with_label (_
                                         ("Force a head revision match if tag/date not found"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_checkout_forcerevision, FALSE,
                        TRUE, 5);
    gtk_widget_show (cb_checkout_forcerevision);
}

void
config_status_build_ui (GtkVBox * vbox)
{
/*        -l      Local directory only, not recursive */
    cb_status_localdir =
        gtk_check_button_new_with_label (_("Local directory only"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_status_localdir, FALSE, TRUE, 5);
    gtk_widget_show (cb_status_localdir);

}

void
config_debug_build_ui (GtkVBox * vbox)
{
/*        -l      Local directory only, not recursive */
    cb_debug_debug = gtk_check_button_new_with_label (_("Debugging output"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_debug_debug, FALSE, TRUE, 5);
    gtk_widget_show (cb_debug_debug);

    cb_debug_show_test_stuff =
        gtk_check_button_new_with_label (_("Show test stuff"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_debug_show_test_stuff, FALSE, TRUE,
                        5);
    gtk_widget_show (cb_debug_show_test_stuff);

}

void
config_update_build_ui (GtkVBox * vbox)
{
/* These are actions, you use them 'once' */
/*        -r rev  Check out revision or tag. (implies -P) (is sticky) */
/*        -D date Check out revisions as of date. (implies -P) (is sticky) */
/*        -d dir  Check out into dir instead of module name. */
/*        -j rev  Merge in changes made between current revision and rev. */
/*        -A      Reset any sticky tags/date/kopts. */


    cb_update_createdirs =
        gtk_check_button_new_with_label (_("Create directories"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_update_createdirs, FALSE, TRUE, 5);
    gtk_widget_show (cb_update_createdirs);
/*        -P      Prune empty directories. */
    cb_update_pruneempty =
        gtk_check_button_new_with_label (_("Prune empty directories"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_update_pruneempty, FALSE, TRUE, 5);
    gtk_widget_show (cb_update_pruneempty);
/*        -l      Local directory only, not recursive */
    cb_update_localdir =
        gtk_check_button_new_with_label (_("Local directory only"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_update_localdir, FALSE, TRUE, 5);
    gtk_widget_show (cb_update_localdir);
/*        -n      Do not run module program (if any). */
    cb_update_runprog =
        gtk_check_button_new_with_label (_("Run module program"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_update_runprog, FALSE, TRUE, 5);
    gtk_widget_show (cb_update_runprog);
/*        -f      Force a head revision match if tag/date not found. */
    cb_update_forcerevision =
        gtk_check_button_new_with_label (_
                                         ("Force a head revision match if tag/date not found"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_update_forcerevision, FALSE, TRUE,
                        5);
    gtk_widget_show (cb_update_forcerevision);

}

void
config_commit_build_ui (GtkVBox * vbox)
{
/*        -l      Local directory only, not recursive */
    cb_commit_localdir =
        gtk_check_button_new_with_label (_("Local directory only"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_commit_localdir, FALSE, TRUE, 5);
    gtk_widget_show (cb_commit_localdir);
/*        -n      Do not run module program (if any). */
    cb_commit_runprog =
        gtk_check_button_new_with_label (_("Run module program"));
    gtk_box_pack_start (GTK_BOX (vbox), cb_commit_runprog, FALSE, TRUE, 5);
    gtk_widget_show (cb_commit_runprog);
    /* These are actions, you use them 'once' */
/*        -f      Force the file to be committed; disables recursion. */
/*        -m msg  Log message. */
/*        -r rev  Commit to this branch or trunk revision. */


}

static void
prefs_build_ui (PharmacyConfigDlg * dlg)
{
    GtkWidget *notebook;
    GtkWidget *vbox;
    GtkWidget *_vbox;
    GtkWidget *hbox;
    GtkWidget *hsep;
    GtkWidget *button;
    GtkWidget *label;
    GtkWidget *book_vbox;
    GtkWidget *frame;
    GtkObject *adjustment;

    dlg->dialog = GNOME_DIALOG (gnome_dialog_new (_("Preferences"),
                                                  //GNOME_STOCK_BUTTON_APPLY,
                                                  GNOME_STOCK_BUTTON_OK,
                                                  GNOME_STOCK_BUTTON_CANCEL,
                                                  NULL));
    gtk_widget_set_usize (GTK_WIDGET (dlg->dialog), 375, 500);

    vbox = GNOME_DIALOG (dlg->dialog)->vbox;

    notebook = gtk_notebook_new ();
    gtk_notebook_set_scrollable (GTK_NOTEBOOK (notebook), TRUE);
    gtk_notebook_set_show_tabs (GTK_NOTEBOOK (notebook), TRUE);
    gtk_notebook_set_show_border (GTK_NOTEBOOK (notebook), TRUE);

/** Real Preferences **/

    /* Global */

    label = gtk_label_new (_("Global"));
    book_vbox = gtk_vbox_new (FALSE, 0);
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), book_vbox, label);
    gtk_widget_show (book_vbox);
    gtk_widget_show (label);

    config_global_build_ui (GTK_VBOX (book_vbox));

    /* Checkout */

    label = gtk_label_new (_("Checkout"));
    book_vbox = gtk_vbox_new (FALSE, 0);
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), book_vbox, label);
    gtk_widget_show (book_vbox);
    gtk_widget_show (label);

    config_checkout_build_ui (GTK_VBOX (book_vbox));

    /* Status/Commit */
    label = gtk_label_new (_("Status"));
    book_vbox = gtk_vbox_new (FALSE, 0);
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), book_vbox, label);
    gtk_widget_show (book_vbox);
    gtk_widget_show (label);
    config_status_build_ui (GTK_VBOX (book_vbox));

    label = gtk_label_new (_("Commit"));
    book_vbox = gtk_vbox_new (FALSE, 0);
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), book_vbox, label);
    gtk_widget_show (book_vbox);
    gtk_widget_show (label);
    config_commit_build_ui (GTK_VBOX (book_vbox));

    label = gtk_label_new (_("Update"));

    book_vbox = gtk_vbox_new (FALSE, 0);
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), book_vbox, label);
    gtk_widget_show (book_vbox);
    gtk_widget_show (label);
    config_update_build_ui (GTK_VBOX (book_vbox));

    label = gtk_label_new (_("Debug"));

    book_vbox = gtk_vbox_new (FALSE, 0);
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), book_vbox, label);
    gtk_widget_show (book_vbox);
    gtk_widget_show (label);

    config_debug_build_ui (GTK_VBOX (book_vbox));

    gtk_signal_connect (GTK_OBJECT (dlg->dialog), "clicked",
                        GTK_SIGNAL_FUNC (config_dialog_button_clicked),
                        (gpointer *) dlg);

    /* TODO */
/*        -I ign  More files to ignore (! to reset). *///list?
/*        -W spec Wrappers specification line. *///clist?

    /* These are actions, you use them 'once' */
/*        -r rev  Check out revision or tag. (implies -P) (is sticky) */
/*        -D date Check out revisions as of date. (implies -P) (is sticky) */
/*        -j rev  Merge in changes made between current revision and rev. */
/*        -A      Reset any sticky tags/date/kopts. */

    gtk_box_pack_start (GTK_BOX (vbox), notebook, TRUE, TRUE, 5);
    gtk_widget_show (notebook);


}

void
preferences_cb ()
{

    PharmacyConfigDlg *dlg = pharmacy_config_dlg_new ();
    g_return_if_fail (cfg != NULL);


#define ACT_TOGGLE(o)   if (cfg-> o){gtk_widget_activate (cb_##o);}

/*  if (cfg->shorten) {
   gtk_widget_activate(cb_shorten);
   }
 */

    ACT_TOGGLE (read_cvsrc);
    ACT_TOGGLE (fakewrite);
    ACT_TOGGLE (logcommand);
    ACT_TOGGLE (readonly);
    ACT_TOGGLE (bEncrypt);

    gtk_entry_set_text (GTK_ENTRY (e_bindir), cfg->bindir);
    gtk_entry_set_text (GTK_ENTRY (e_editor), cfg->editor);
    gtk_entry_set_text (GTK_ENTRY (e_tmpdir), cfg->tmpdir);

    ACT_TOGGLE (checkout_shortenpath);
    ACT_TOGGLE (checkout_pruneempty);
    ACT_TOGGLE (checkout_localdir);
    ACT_TOGGLE (checkout_runprog);
    ACT_TOGGLE (checkout_forcerevision);
    ACT_TOGGLE (status_localdir);
    ACT_TOGGLE (update_createdirs);
    ACT_TOGGLE (update_pruneempty);
    ACT_TOGGLE (update_localdir);
    ACT_TOGGLE (update_runprog);
    ACT_TOGGLE (update_forcerevision);
    ACT_TOGGLE (commit_localdir);
    ACT_TOGGLE (commit_runprog);
    ACT_TOGGLE (debug_debug);
    ACT_TOGGLE (debug_show_test_stuff);

    gtk_widget_show (GTK_WIDGET (dlg->dialog));
}

#define BOOLEAN_OPTION(a,b) ((a)?(b):"")
gchar *
cvs_checkout_get_options ()
{
    gchar *pRet = NULL;
    pRet = g_strdup_printf ("%s%s%s%s%s",
                            BOOLEAN_OPTION (!cfg->checkout_shortenpath,
                                            "-N "),
                            BOOLEAN_OPTION (cfg->checkout_pruneempty, "-P "),
                            BOOLEAN_OPTION (cfg->checkout_localdir, "-l "),
                            BOOLEAN_OPTION (!cfg->checkout_runprog, "-n "),
                            BOOLEAN_OPTION (cfg->checkout_forcerevision,
                                            "-f "));
    return pRet;
}

gchar *
cvs_commit_get_options ()
{
    gchar *pRet = NULL;

    pRet = g_strdup_printf ("%s%s" /*"%s%s%s%s%s" */ ,
                            BOOLEAN_OPTION (cfg->commit_localdir, "-l "),
                            BOOLEAN_OPTION (!cfg->commit_runprog, "-n "));
    return pRet;
}

gchar *
cvs_global_get_options ()
{
    gboolean read_cvsrc, fakewrite, logcommand, readonly, bEncrypt;
    gint compression;
    gchar *pRet = NULL;
    gchar *pCompression = NULL;
    gchar *pEditor = NULL;
    gchar *pRCS = NULL;
    gchar *pTMP = NULL;

    /* Temp use */
    gchar pBuffer[30];
    pBuffer[0] = (gchar) (cfg->compression_level) + '0';
    pBuffer[1] = 0;
    pCompression = g_strconcat ("-z", pBuffer, " ", NULL);

    if (cfg->editor && strlen (cfg->editor)) {
        pEditor = g_strconcat ("-e \"", cfg->editor, "\" ", NULL);
    }

    if (cfg->bindir && strlen (cfg->bindir)) {
        pRCS = g_strconcat ("-b \"", cfg->bindir, "\" ", NULL);
    }


    if (cfg->tmpdir && strlen (cfg->tmpdir)) {
        pTMP = g_strconcat ("-T \"", cfg->tmpdir, "\" ", NULL);
    }



    pRet = g_strdup_printf ("%s%s%s%s%s%s%s%s%s",
                            STRING_PROTECT (pCompression),
                            STRING_PROTECT (pEditor),
                            STRING_PROTECT (pRCS),
                            STRING_PROTECT (pTMP),
                            BOOLEAN_OPTION (!cfg->read_cvsrc, "-f "),
                            BOOLEAN_OPTION (cfg->fakewrite, "-n "),
                            BOOLEAN_OPTION (!cfg->logcommand, "-l "),
                            BOOLEAN_OPTION (cfg->readonly, "-r "),
                            BOOLEAN_OPTION (cfg->bEncrypt, "-x "));

    g_free (pEditor);
    g_free (pRCS);
    g_free (pTMP);
    g_free (pCompression);

    return pRet;
}

gchar *
cvs_status_get_options ()
{
    gchar *pRet = NULL;

    pRet = g_strdup_printf ("%s",
                            BOOLEAN_OPTION (cfg->status_localdir, "-l "));

    return pRet;
}

gchar *
cvs_update_get_options ()
{
    gchar *pRet = NULL;

    pRet = g_strdup_printf ("%s%s%s%s%s",
                            BOOLEAN_OPTION (cfg->update_createdirs, "-d "),
                            BOOLEAN_OPTION (cfg->update_pruneempty, "-P "),
                            BOOLEAN_OPTION (cfg->update_localdir, "-l "),
                            BOOLEAN_OPTION (!cfg->update_runprog, "-n "),
                            BOOLEAN_OPTION (cfg->update_forcerevision,
                                            "-f "));
    return pRet;
}

PharmacyConfigDlg *
pharmacy_config_dlg_new ()
{
    PharmacyConfigDlg *dlg = g_malloc (sizeof (PharmacyConfigDlg));

    prefs_build_ui (dlg);


    return dlg;
}
