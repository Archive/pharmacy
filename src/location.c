/* location.c
   This file is part of "Pharmacy: A GNOME CVS front-end"
   Copyright 1998 Reklaw (N. Adam Walker)
   "Pharmacy" is released under the GPL
   See the LICENCE for more details.
 */

#include <gnome.h>
#include <assert.h>

#include "location.h"

LocationData *
locationdata_new_with_values (const gchar * name,
			      const gchar * server,
			      const gchar * workdir)
{
	LocationData *pRet;

	pRet = g_malloc (sizeof (LocationData));
	locationdata_set_values (pRet, name, server, workdir);
	pRet->nMark = 0;

	return pRet;
}

void 
locationdata_set_values (LocationData * pRet,
			 const gchar * name,
			 const gchar * server,
			 const gchar * workdir)
{
	/* set name */
	if (name)
		pRet->pgcName = g_strdup (name);
	else
		pRet->pgcName = g_strdup ("");

	/* set server*/
	if (server)
		pRet->pgcServer = g_strdup (server);
	else
		pRet->pgcServer = g_strdup ("");

	/* set workdir */
	if(workdir)
	{
		if (workdir[strlen (workdir) - 1] == '/')
			pRet->pgcWorkDir = g_strdup (workdir);
		else
			pRet->pgcWorkDir = g_strconcat (workdir, "/", NULL);
	}
	else
		pRet->pgcWorkDir = g_strdup ("/");	
}

void 
locationdata_free (gpointer pData)
{
	LocationData *pLoc;
  
	if (pData)
	{
		pLoc = (LocationData *) pData;
  
		g_free (pLoc->pgcServer);
		g_free (pLoc->pgcWorkDir);
		g_free (pLoc->pgcName);
		g_free (pLoc);
	}
}

const gchar *
locationdata_get_server (gpointer p)
{
  LocationData *pData = (LocationData *) p;
  assert (p);

  return pData->pgcServer;
}

const gchar *
locationdata_get_workdir (gpointer p)
{
  LocationData *pData = (LocationData *) p;
  assert (p);

  return pData->pgcWorkDir;
}

const gchar *
locationdata_get_name (gpointer p)
{
  LocationData *pData = (LocationData *) p;
  assert (p);

  return pData->pgcName;
}


/* Locations List */
void 
locations_load_list (GList * pList)
{
  gint count;
  gint wdcount;
  gint scount;
  gint ncount;
  gchar **workdirs = NULL;
  gchar **servers = NULL;
  gchar **names = NULL;
  LocationData *pLoc = NULL;
  gint i;
  count = gnome_config_get_int (CONFIG_LOCATION_COUNT);

  if (count)
    {
      gnome_config_get_vector (CONFIG_LOCATION_WORKDIRS, &wdcount, &workdirs);
      gnome_config_get_vector (CONFIG_LOCATION_SERVERS, &scount, &servers);
      gnome_config_get_vector (CONFIG_LOCATION_NAMES, &ncount, &names);
      for (i = 0; i < count; i++)
	{
	  if (i > wdcount || i > scount || !scount || !wdcount)
	    {
	      break;
	    }
	  else
	    {
	      pLoc = locationdata_new_with_values (i < ncount ? names[i] : NULL,
						   servers[i],
						   workdirs[i]);
	      g_list_append (pList, pLoc);
	    }
	}
    }
}

typedef struct _SaveList SaveList;
struct _SaveList
  {
    gint count;
    gint allocated;
    char **workdirs;
    char **servers;
    char **names;
  };

gint 
savelist_realloc (SaveList * psl)
{
  gchar **w;
  gchar **s;
  gchar **n;
  gint bRet = FALSE;
  assert (psl);

  w = g_realloc (psl->workdirs, sizeof (gchar *) * psl->allocated);
  s = g_realloc (psl->servers, sizeof (gchar *) * psl->allocated);
  n = g_realloc (psl->names, sizeof (gchar *) * psl->allocated);

  if (w && s && n)
    {
      psl->workdirs = w;
      psl->servers = s;
      psl->names = n;

      bRet = TRUE;
    }
  return bRet;
}

static void 
save_list_cb (gpointer pData, gpointer pSl)
{
  SaveList *psl = (SaveList *) pSl;
  gint bCont = TRUE;
  LocationData *pLoc = (LocationData *) pData;

  if (pLoc == NULL)
    bCont = FALSE;

  if (bCont)
    {

      if (psl->count == psl->allocated)
	{
	  psl->allocated += 5;
	  if (!savelist_realloc (psl))
	    bCont = FALSE;
	}

      if (bCont)
	{
	  psl->workdirs[psl->count] = pLoc->pgcWorkDir;
	  psl->servers[psl->count] = pLoc->pgcServer;
	  psl->names[psl->count] = pLoc->pgcName;

	  psl->count++;
	}
    }
}

void 
locations_save_list (GList * pList)
{
  SaveList sl;

  sl.count = 0;
  sl.allocated = 5;
  sl.workdirs = NULL;
  sl.servers = NULL;
  sl.names = NULL;

  if (g_list_first(pList) && savelist_realloc (&sl))
    {
      g_list_foreach (pList, save_list_cb, &sl);

      gnome_config_set_int (CONFIG_LOCATION_COUNT, sl.count);
      if (sl.count)
	{
	  gnome_config_set_vector (CONFIG_LOCATION_WORKDIRS, sl.count,
				   (const char **) sl.workdirs);
	  gnome_config_set_vector (CONFIG_LOCATION_SERVERS, sl.count,
				   (const char **) sl.servers);
	  gnome_config_set_vector (CONFIG_LOCATION_NAMES, sl.count,
				   (const char **) sl.names);
	}
    }

  g_free(sl.workdirs);
  g_free(sl.servers);
  g_free(sl.names);

}

#define ACTION_CLEAR 0
#define ACTION_REMOVE 1

typedef struct _MarkData MarkData;
struct _MarkData
  {
    gint action;
    GList *pList;
    GList *pRemove;
  };

static void 
marks_cb (LocationData * pLoc, MarkData * pMarkData)
{
  /*  LocationData* pLoc = (LocationData*)pData;
     MarkData* pMarkData = (MarkData*)pMD; */
  assert (pMarkData);

  if (pMarkData && pLoc)
    {

      switch (pMarkData->action)
	{
	case ACTION_CLEAR:
	  pLoc->nMark = 0;
	  break;
	case ACTION_REMOVE:
	  if (pLoc->nMark)
	    {
	      /*locationdata_free(pLoc); */
	      g_list_append (pMarkData->pRemove, pLoc);
	    }
	  break;
	default:
	  g_warning (_ ("Unknown Action"));
	}
    }
}

static void 
remove_cb (gpointer pData, gpointer pList)
{
  if (pData && pList)
    {
      g_list_remove (pList, pData);
    }
}

void 
locations_clear_marks (GList * pList)
{
  MarkData md;
  md.action = ACTION_CLEAR;
  md.pList = pList;
  g_list_foreach (pList, (GFunc) marks_cb, &md);
}

void 
locations_remove_marked (GList * pList)
{
  MarkData md;
  md.action = ACTION_REMOVE;
  md.pList = pList;
  md.pRemove = g_list_alloc ();
  g_list_foreach (pList, (GFunc) marks_cb, &md);
  g_list_foreach (md.pRemove, remove_cb, pList);
  g_list_free (md.pRemove);
}

void 
locationdata_mark (gpointer p)
{
  LocationData *pLoc = (LocationData *) p;

  pLoc->nMark = 1;

}

LocationData*
locationdata_duplicate (LocationData* pSrc)
{
	LocationData *pDest;

	if(pSrc == NULL)
		pDest == NULL;
	else
	{
		pDest = locationdata_new_with_values (
		          g_strdup (locationdata_get_name(pSrc)),
		          g_strdup (locationdata_get_server(pSrc)),
		          g_strdup (locationdata_get_workdir(pSrc)));
		pDest->nMark = pSrc->nMark;
	}	

	return (pDest);
}