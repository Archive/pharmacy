/* pharmacy.c
   This file is part of "Pharmacy: A GNOME CVS front-end"
   Copyright 1998 Reklaw (N. Adam Walker)
   "Pharmacy" is released under the GPL
   See the LICENCE for more details.
 */
#include <config.h>
#include <gnome.h>

#include "que.h"

QueNode *
quenode_new ()
{
  QueNode *pNode = g_malloc (sizeof (QueNode));

  pNode->pFile = NULL;
  pNode->pDir = NULL;
  pNode->bFinished = FALSE;
  pNode->eType = Type_None;
  pNode->eAction = Action_None;

  return pNode;
}
