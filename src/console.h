#ifndef R_CONSOLE_H
#define R_CONSOLE_H 1

#include "location.h"
#include "dirtree.h"
#include "agent.h"

   typedef struct _CVSConsole CVSConsole;

   struct _CVSConsole
   {
   /* The Layout box */
     GtkWidget* pvBox;
      
     /* Commands output here */
     GtkWidget* pOutput;
     /* Type in the entry box */
     GtkWidget* pCmdLine;
     GtkWidget* pGo;
     GtkWidget* pStop;
     GtkWidget* pClear;
      
      /* State */
      DirTreeNode* pCurrentNode;
      Agent* pAgent;
      gint bGo;
      gint bStop;
   };

   CVSConsole* console_new();
   GtkWidget* console_get_widget(CVSConsole*);
   void console_set_node(CVSConsole* pCVS, DirTreeNode* pNode);
   void console_execute_cmd(const gchar* text);
   void console_execute_cmd_in_dir(const gchar* text,const gchar* dir);
   void go_menu_cb();
   void stop_menu_cb();
   void console_print_cb();
   void console_clear_cb();

#endif
