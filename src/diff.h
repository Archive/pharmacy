#ifndef R_DIFF_H
#define R_DIFF_H 1

#define DC_BUFF_LEN 4048

typedef struct _DiffControl DiffControl;

struct _DiffControl
  {
    /* The Layout box */
    GtkWidget *pvBox;

    /* Commands output here */
    GtkWidget *pList;
    /* Type in the entry box */
    GtkWidget *pFileOneDD;
    GtkWidget *pFileTwoDD;
    GtkWidget *pRefresh;
    GtkWidget *pOpen;

    /* State */
  };

DiffControl *diffcontrol_new ();
GtkWidget *diffcontrol_get_widget (DiffControl *);

#endif

