#include "config.h"
#include <gnome.h>
#include "stock.h"

/* Icons */
#include "redlight.xpm"
#include "bolt.xpm"
#include "checkout.xpm"
#include "cvssm.xpm"
#include "cvsdir.xpm"

void
pharmacy_stock_init()
{
  GnomeStockPixmapEntry* entry;

#define REGISTER_DATA(labeltext,pixmapdata) \
  entry = g_malloc(sizeof(GnomeStockPixmapEntryData));\
  entry->type = entry->data.type = GNOME_STOCK_PIXMAP_TYPE_DATA;\
  entry->data.width = entry->data.height = 24;\
  entry->data.label = g_strdup(labeltext);\
  entry->data.xpm_data = pixmapdata;\
  gnome_stock_pixmap_register(labeltext,\
			      GNOME_STOCK_PIXMAP_REGULAR,\
			      entry)

  REGISTER_DATA(PHARMACY_STOCK_PIXMAP_CHECKOUT,checkout_xpm);
  REGISTER_DATA(PHARMACY_STOCK_PIXMAP_BOLT,bolt_xpm);
  REGISTER_DATA(PHARMACY_STOCK_PIXMAP_FISH,cvssm_xpm);
  REGISTER_DATA(PHARMACY_STOCK_PIXMAP_CVSDIR,cvsdir_xpm);
  
}

