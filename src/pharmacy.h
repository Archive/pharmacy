#ifndef PHARMACY_H
#define PHARMACY_H 1

#include <gnome.h>

#include "utils.h"

#include "console.h"
#include "agent.h"
#include "filelist.h"
#include "diff.h"

GList *pharmacy_get_locations ();

#define PHARMACY(obj) GTK_CHECK_CAST(obj,pharmacy_get_type(), Pharmacy)
#define PHARMACY_CLASS(klass) GTK_CHECK_CAST_CLASS(klass, pharmacy_get_type())
#define IS_PHARMACY(obj) GTK_CHECK_TYPE(obj, pharmacy_get_type())

#define PHARMACY_UI_UPDATE_SIGNAL "ui_update"

typedef struct _Pharmacy Pharmacy;
typedef struct _PharmacyClass PharmacyClass;

struct _Pharmacy
  {
    GnomeApp app;
    GtkWidget *hpane;
    GtkWidget *vpane;
    GtkWidget *dirtree;
    GtkWidget *filelist;
    GtkWidget *buildlist;
    GtkWidget *statusbar;
    GtkWidget *notebook;
    CVSConsole *cvscon;
    DiffControl *diff;
    GtkWidget *log;
    GtkWidget *que;
    GtkWidget *dir_label;

    GList *pLocations;

  };

struct _PharmacyClass
  {
    GnomeAppClass parent_class;

    void (*ui_update) (gchar * pgcHint);
  };

guint pharmacy_get_type ();
GtkWidget *pharmacy_new ();

void pharmacy_nop ();
CVSConsole *pharmacy_get_console ();
FileList *pharmacy_get_filelist ();
GnomeAppBar *pharmacy_get_appbar ();
GnomeApp *pharmacy_get_app ();
GtkCTree *pharmacy_get_dirtree ();
void pharmacy_rebuild_entire_dirtree ();
void pharmacy_update_ui (gchar * pgcHint);
Agent *pharmacy_get_agent ();
DiffControl *pharmacy_get_diff();

void pharmacy_color_init (void);
void pharmacy_set_locations (GList*);

extern GnomeUIInfo toolbar[];
extern GnomeUIInfo program_menu[];
extern GnomeUIInfo view_menu[];
extern GnomeUIInfo repository_menu[];
extern GnomeUIInfo tools_menu[];
extern GnomeUIInfo console_menu[];
extern GnomeUIInfo main_menu[];
#endif /* PHARMACY_H */
