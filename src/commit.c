/* commit.c
   This file is part of "Pharmacy: A GNOME CVS front-end"
   Copyright 1998 Reklaw (N. Adam Walker)
   "Pharmacy" is released under the GPL
   See the LICENCE for more details.
 */
#include <config.h>
#include <gnome.h>

#include "pharmacy.h"
#include "console.h"
#include "preferences.h"

static gint aSignals[2];
static gchar *pgcCommitBuffer = NULL;
static gint bCommit = FALSE;

#define MENU 11
#define TOOLBAR 4

void 
commit_ui_update_cb (gchar * pgcHint)
{
  CVSConsole *pCon = pharmacy_get_console ();

  if (pCon && pCon->pCurrentNode &&
      !agent_in_use (pharmacy_get_agent ()))
    bCommit = TRUE;
  else
    bCommit = FALSE;

  gtk_widget_set_sensitive (repository_menu[MENU].widget, bCommit);
  gtk_widget_set_sensitive(toolbar[TOOLBAR].widget,bCommit);

}

static void 
commit_done (Agent * pAgent)
{
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[0]);
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[1]);

  g_free (pgcCommitBuffer);
  pgcCommitBuffer = NULL;

  filelist_rebuild_list (pharmacy_get_filelist (),
			 pharmacy_get_console ()->pCurrentNode);

  gnome_appbar_pop (pharmacy_get_appbar ());
}

static void 
commit_read (Agent * pAgent, gchar * str, gint count)
{
  concat_to_buffer (&pgcCommitBuffer, str);

}

void 
commit_cb ()
{

  Agent *pAgent = pharmacy_get_console ()->pAgent;
  gchar *pCmd = NULL;
  gchar *pGlobalOptions = NULL;
  gchar *pFiles = NULL;

  g_return_if_fail (bCommit);
  aSignals[0] = gtk_signal_connect (GTK_OBJECT (pAgent),
				    "child-died",
				    commit_done,
				    NULL);

  aSignals[1] = gtk_signal_connect (GTK_OBJECT (pAgent),
				    "read-child-output",
				    commit_read,
				    NULL);

  gnome_appbar_push (pharmacy_get_appbar (),
		     _ ("Performing commit..."));


  pGlobalOptions = cvs_global_get_options ();

  pFiles = filelist_get_selected_files (pharmacy_get_filelist ());
  pCmd = g_strconcat ("cvs ", pGlobalOptions,
		      "commit ",
		      pFiles ? pFiles : "",
		      NULL);
  console_execute_cmd (pCmd);

  g_free (pGlobalOptions);
  g_free (pCmd);
  g_free (pFiles);
}
