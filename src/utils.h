#ifndef R_UTILS_H

extern gint g_bDebug;
extern gint g_bExper;

GtkWidget *util_contain_in_scrolled (GtkWidget * pWidget);

void
g_debug_message(const gchar* format, ...);


/* Debug version functions */
#define g_debug_message2(a,b) g_debug_message(a,b)
#define g_debug_message3(a,b,c) g_debug_message(a,b,c)

#define g_debug_message1(a) g_debug_message(a)

#endif /* R_UTILS_H */
/* Last line. DO NOT ADD ANY CODE BELOW THIS LINE  */





