#ifndef LOCATION_H
#define LOCATION_H 1

   typedef struct _LocationData LocationData;
   struct _LocationData
   {
   
   /* Saveable Data */
      gchar* pgcWorkDir;
      gchar* pgcName;
      gchar* pgcServer;
      
      /* Internal Use */
      gint nMark;
   
   };

   LocationData* locationdata_new_with_values(const gchar* naem,
   const gchar* server,
   const gchar* workdir);
   void locationdata_set_values(LocationData* pRet,
	const gchar* naem,
   const gchar* server,
   const gchar* workdir);

   void locationdata_free(gpointer p);
   const gchar* locationdata_get_workdir(gpointer p);
   const gchar* locationdata_get_server(gpointer p);
   const gchar* locationdata_get_name(gpointer p);
   void locationdata_mark(gpointer p);

   void locations_load_list(GList* pList);
   void locations_save_list(GList* pList);

   void locations_clear_marks(GList* pList);
   void locations_remove_marked(GList* pList);

	LocationData* locationdata_duplicate (LocationData*);
	
#define CONFIG_LOCATION_COUNT "/pharmacy/locations/count"
#define CONFIG_LOCATION_WORKDIRS "/pharmacy/locations/workdirs"
#define CONFIG_LOCATION_SERVERS "/pharmacy/locations/servers"
#define CONFIG_LOCATION_NAMES "/pharmacy/locations/names"

#endif /* LOCATION_H */

