/* login.c
   This file is part of "Pharmacy: A GNOME CVS front-end"
   Copyright 1998 Reklaw (N. Adam Walker)
   "Pharmacy" is released under the GPL
   See the LICENCE for more details.
 */
#include "config.h"
#include <gnome.h>

#include "pharmacy.h"
#include "agent.h"
#include "login.h"

#define LOOK_FOR_PHARSE "CVS password: "
#define LOOK_FOR_FAIL "incorrect password"

static gchar *pgcLoginBuffer = NULL;
static gint aLoginSignals[2];
static gint bLogin = FALSE;

#define MENU 2

void 
login_ui_update_cb (gchar * pgcHint)
{
  CVSConsole *pCon = pharmacy_get_console ();
  if (pCon && pCon->pCurrentNode && pCon->pCurrentNode->pData &&
      !agent_in_use (pharmacy_get_agent ()))
    bLogin = TRUE;
  else
    bLogin = FALSE;

  gtk_widget_set_sensitive (repository_menu[MENU].widget, bLogin);

  /* FIXME: Soon, the 'tool' menu will work and this
     should be removed */
  gtk_widget_set_sensitive (tools_menu[0].widget, FALSE);
  gtk_widget_set_sensitive (tools_menu[1].widget, FALSE);
  //      gtk_widget_set_sensitive(tools_menu[3].widget,FALSE);
}

static void 
login_reply_cb (gchar * str, gpointer dontcare)
{
  gchar *pBuffer = NULL;
  CVSConsole *pConsole = pharmacy_get_console ();

  pBuffer = g_strconcat (str, "\r\n", NULL);

  agent_writechild (pConsole->pAgent, pBuffer, strlen (pBuffer));
}

static void 
login_agent_read (Agent * pAgent, gchar * str, gint count)
{
  GtkWidget *pDlg = NULL;

  concat_to_buffer (&pgcLoginBuffer, str);

  if (strstr (pgcLoginBuffer, LOOK_FOR_PHARSE))
    {
      g_free (pgcLoginBuffer);
      pgcLoginBuffer = NULL;

      /* Ask for password */
      pDlg = gnome_app_request_password (pharmacy_get_app (),
		  _ ("Please enter your password for the selected server."),
      /*cb */ login_reply_cb,
      /*data */ NULL);
    }
  else
    {

      if (strstr (pgcLoginBuffer, LOOK_FOR_FAIL))
	{
	  g_free (pgcLoginBuffer);
	  pgcLoginBuffer = NULL;
	  gnome_app_message (pharmacy_get_app (),
			     _ ("You entered an incorrect password."));
	}
    }
}

static void 
login_agent_done (Agent * pAgent)
{
  g_assert (IS_AGENT (pAgent));

  if (pgcLoginBuffer)
    {
      g_free (pgcLoginBuffer);
      pgcLoginBuffer = NULL;
    }

  gtk_signal_disconnect (GTK_OBJECT (pAgent), aLoginSignals[0]);
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aLoginSignals[1]);

  gnome_appbar_pop (pharmacy_get_appbar ());
}

void 
login_cb ()
{
  Agent *pAgent = pharmacy_get_console ()->pAgent;
  gchar *cmd = NULL;
  LocationData *pLoc = NULL;

  if (bLogin)
    {

      aLoginSignals[0] = gtk_signal_connect (GTK_OBJECT (pAgent),
					     AGENT_READ_CHILD_OUTPUT_SIGNAL,
					     login_agent_read,
					     NULL);

      aLoginSignals[1] = gtk_signal_connect (GTK_OBJECT (pAgent),
					     AGENT_CHILD_DIED_SIGNAL,
					     login_agent_done,
					     NULL);

      gnome_appbar_push (pharmacy_get_appbar (),
			 _ ("Perfoming login... "));



      pLoc = (LocationData *) (pharmacy_get_console ()->pCurrentNode->pData);

      g_assert (pLoc);


      cmd = g_strconcat ("cvs -d ",
			 locationdata_get_server (pLoc),
			 " login", NULL);

      console_execute_cmd (cmd);

      g_free (cmd);
    }
}
