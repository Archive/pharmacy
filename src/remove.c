/* remove.c
   This file is part of "Pharmacy: A GNOME CVS front-end"
   Copyright 1998 Reklaw (N. Adam Walker)
   "Pharmacy" is released under the GPL
   See the LICENCE for more details.
 */
#include <config.h>
#include <gnome.h>

#include "pharmacy.h"
#include "console.h"
#include "preferences.h"

static gint aSignals[2];
static gchar *pgcRemoveBuffer = NULL;
static gint bRemove = TRUE;
static gint bRecover = TRUE;

#define REMOVE_MENU 8
#define REMOVE_TOOLBAR 5
#define RECOVER_MENU 9
#define RECOVER_TOOLBAR 6

typedef struct _RecoverData RecoverData;

struct _RecoverData
{
  gboolean b;
  gint count;
};

static void
detect_removed_cb(gpointer pProtoRow,gpointer pData)
{
  const gint nRow = (gint)pProtoRow;
  const FileListNode* pNode = NULL;
  RecoverData* recover = (RecoverData*)pData;
  gchar* version = NULL;

  g_debug_message("In DetectRemoved row is %d",nRow);

  if(recover && recover->b)
    {
      g_debug_message("passed b && *b");

      /*  pNode = gtk_clist_get_row_data(GTK_CLIST(pharmacy_get_filelist()),
	  nRow);
	  
	  g_return_if_fail(pNode);
	  g_debug_message("Have node");
      */

      gtk_clist_get_text(GTK_CLIST(pharmacy_get_filelist()),nRow,1,&version);
      
      g_return_if_fail(version);
      g_debug_message("Have version");
      
      if(version[0]!='-')
	{
	  g_debug_message("It's here. not removed.");
	  g_debug_message("verison is \"%s\"",version);
	  recover->b = FALSE;
	}
      else
	{
	  g_debug_message("it has been removed");
	  recover->count ++;
	}
    }
}

void 
remove_ui_update_cb (gchar * pgcHint)
{
  CVSConsole *pCon = pharmacy_get_console ();
  RecoverData recover;

  if (pCon && pCon->pCurrentNode &&
      !agent_in_use (pharmacy_get_agent ()))
    bRemove = TRUE;
  else
    bRemove = FALSE;

  if(bRemove)
    {
      recover.b = TRUE;
      recover.count = 0;
      g_list_foreach(GTK_CLIST(pharmacy_get_filelist())->selection,
		     detect_removed_cb,&recover);
      bRecover = recover.count?recover.b:FALSE;

      if(bRecover)
	{
	  bRemove = FALSE;
	}
    }
  else
    {
      bRecover = FALSE;
    }

  gtk_widget_set_sensitive (repository_menu[REMOVE_MENU].widget, bRemove);
  gtk_widget_set_sensitive(toolbar[REMOVE_TOOLBAR].widget,bRemove);

  gtk_widget_set_sensitive (repository_menu[RECOVER_MENU].widget, bRecover);
  gtk_widget_set_sensitive(toolbar[RECOVER_TOOLBAR].widget,bRecover);

}

static void 
remove_done (Agent * pAgent)
{
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[0]);
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[1]);

  g_free (pgcRemoveBuffer);
  pgcRemoveBuffer = NULL;

  filelist_rebuild_list (pharmacy_get_filelist (),
			 pharmacy_get_console ()->pCurrentNode);

  gnome_appbar_pop (pharmacy_get_appbar ());
  pharmacy_update_ui("remove-done");
}

static void 
remove_read (Agent * pAgent, gchar * str, gint count)
{
  concat_to_buffer (&pgcRemoveBuffer, str);

}

void 
remove_cb ()
{

  Agent *pAgent = pharmacy_get_agent ();
  gchar *pCmd = NULL;
  gchar *pGlobalOptions = NULL;
  gchar *pFiles = NULL;
  gchar *pDir = NULL;
  gint i = 0;
  gboolean bDir = FALSE;

  gdk_flush();

  g_return_if_fail (bRemove);

  pFiles = filelist_get_selected_files (pharmacy_get_filelist ());
  pDir = g_strdup (
		    pharmacy_get_console ()->pCurrentNode->pgcDir);

  g_return_if_fail (pDir);

  if (!pFiles)
    {
      bDir = TRUE;
      pFiles = g_strdup (
			  pharmacy_get_console ()->pCurrentNode->pgcName);

      /* Parent dir */
      for (i = strlen (pDir) - 2; i >= 0; i--)
	{
	  if (pDir[i] == '/')
	    {
	      pDir[i + 1] = 0;
	      break;
	    }
	}
    }


  aSignals[0] = gtk_signal_connect (GTK_OBJECT (pAgent),
				    "child-died",
				    remove_done,
				    NULL);

  aSignals[1] = gtk_signal_connect (GTK_OBJECT (pAgent),
				    "read-child-output",
				    remove_read,
				    NULL);

  gnome_appbar_push (pharmacy_get_appbar (),
		     _ ("Performing remove..."));


  pGlobalOptions = cvs_global_get_options ();

  if (bDir)
    {
      /* FIXME: Remove the directory? */
      /* FIXME: The damn thing can't recursely delete first */
      pCmd = g_strconcat ("rm -f ", pFiles,
			  "/* ; ",
			  "cvs ", pGlobalOptions,
			  "remove ",
			  pFiles,
			  NULL);
    }
  else
    {
      /* CVS complains if the files still exist */
      pCmd = g_strconcat ("rm -rf ", pFiles, " ; ",
			  "cvs ", pGlobalOptions,
			  "remove ",
			  pFiles,
			  NULL);
    }

  console_execute_cmd_in_dir (pCmd, pDir);

  g_free (pGlobalOptions);
  g_free (pCmd);
  g_free (pFiles);
  g_free (pDir);
}

static void
mk_sed_cb(gpointer pProtoRow,gpointer pData)
{
  gchar* pSedCmdIn = *((gchar**)pData);
  gchar* pSedCmdOut = NULL;
  FileList* pList = pharmacy_get_filelist ();
  const gint nRow = (gint)pProtoRow;
  const FileListNode* pNode = gtk_clist_get_row_data(GTK_CLIST(pList),nRow);
  gchar* filename = NULL;

  g_return_if_fail(pNode);

  filename = pNode->pgcName;

  g_return_if_fail(filename);

  if(pSedCmdIn)
    {
      pSedCmdOut = g_strconcat(pSedCmdIn, " -e s%",filename,"/-%",
			       filename,"/%",NULL);
      g_free(pSedCmdIn);
    }
  else
    {
      pSedCmdOut = g_strconcat("sed -e s%",filename,"/-%",
			       filename,"/%",NULL);
    }

  *((gchar**)pData) = pSedCmdOut;
}


void 
remove_recover_cb ()
{

  Agent *pAgent = pharmacy_get_agent ();
  gchar *pCmd = NULL;
  gchar *pGlobalOptions = NULL;
  gchar *pUpdateOptions = NULL;
  gchar *pFiles = NULL;
  gchar *pDir = NULL;
  gchar *pSedCmd = NULL;
  gint i = 0;
  gboolean bDir = FALSE;

  gdk_flush();

  g_return_if_fail (bRecover);

  pFiles = filelist_get_selected_files (pharmacy_get_filelist ());
  g_return_if_fail (pFiles);
  pDir = g_strdup (
		    pharmacy_get_console ()->pCurrentNode->pgcDir);

  g_return_if_fail (pDir);


  aSignals[0] = gtk_signal_connect (GTK_OBJECT (pAgent),
				    "child-died",
				    remove_done,
				    NULL);

  aSignals[1] = gtk_signal_connect (GTK_OBJECT (pAgent),
				    "read-child-output",
				    remove_read,
				    NULL);

  gnome_appbar_push (pharmacy_get_appbar (),
		     _ ("Recovering files..."));

  g_list_foreach(GTK_CLIST(pharmacy_get_filelist())->selection,
		 mk_sed_cb, &pSedCmd);

  g_return_if_fail(pSedCmd);


  pGlobalOptions = cvs_global_get_options ();
  pUpdateOptions = cvs_update_get_options ();

  /* FIXME: use sed to change -version back to version */
  pCmd = g_strconcat (pSedCmd,
		      " CVS/Entries > CVS/Entries.new &&" \
		      " mv -f CVS/Entries.new CVS/Entries && ",
		      "cvs ", pGlobalOptions,
		      "update ", pUpdateOptions,
		      pFiles,
		      NULL);

  console_execute_cmd_in_dir (pCmd, pDir);

  g_free (pGlobalOptions);
  g_free (pUpdateOptions);
  g_free (pSedCmd);
  g_free (pCmd);
  g_free (pFiles);
  g_free (pDir);
}
