#ifndef R_AGENT_H
#define R_AGENT_H 1

#define AGENT_STREAM_SIGNAL "agent_stream"
#define AGENT_CHILD_DIED_SIGNAL "child_died"
#define AGENT_READ_CHILD_ERROR_SIGNAL "read_child_error"
#define AGENT_READ_CHILD_OUTPUT_SIGNAL "read_child_output"

#define AGENT(obj) GTK_CHECK_CAST(obj,agent_get_type(), Agent)
#define AGENT_CLASS(klass) GTK_CHECK_CAST_CLASS(klass, agent_get_type())
#define IS_AGENT(obj) GTK_CHECK_TYPE(obj, agent_get_type())

   typedef struct _Agent Agent;
   typedef struct _AgentClass AgentClass;

#define READ 0
#define WRITE 1

   struct _Agent
   {
      GtkObject parent;
      
      gint childpid;
      gint childfd;
      gint msgfd;
      
      gint input_id;
      gint msg_id;
   };

   struct _AgentClass
   {
      GtkObjectClass parent_object;
      
      void (*read_child_output) (Agent* agent, gchar* str, gint len);
      void (*read_child_error) (Agent* agent, gchar* str, gint len);
      void (*child_died) (Agent* agent);
      void (*agent_stream) (Agent* agent, gchar* str, gint len);
   };

   guint agent_get_type();
   GtkObject* agent_new();

	/* Runs the cmd. This function should return immeadiatly */
   void agent_run(Agent* agent, gchar* cmd);
   int agent_writechild(Agent* agent, gchar* buffer, gint len);
   int agent_readchild(Agent* agent, gchar* buffer, gint len);
   int agent_closepty(Agent* agent);
   int agent_abort(Agent* agent);
   gint agent_in_use(Agent* pAgent);
   gchar* concat_to_buffer(gchar** pBuffer, gchar* str);
#endif /*R_AGENT_H*/
