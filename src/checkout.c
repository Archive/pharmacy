/* checkout.c
   This file is part of "Pharmacy: A GNOME CVS front-end"
   Copyright 1998 Reklaw (N. Adam Walker)
   "Pharmacy" is released under the GPL
   See the LICENCE for more details.
 */

#include <config.h>
#include <gnome.h>

#include "console.h"
#include "checkout.h"
#include "pharmacy.h"
#include "location.h"
#include "preferences.h"

#define LOOK_FOR_ABORT "checkout aborted"

static gchar *pgcCheckoutBuffer = NULL;

static gint aSignals[2];
static gint bCheckout = FALSE;

static GtkWidget *pModuleList = NULL;
static GtkWidget *pModuleEntry = NULL;
static GtkWidget *pCheckoutDialog = NULL;

/* Menu Stuff */
#define MENU 6
#define TOOLBAR 2

/* Dialog Buttons */
#define UPDATE_LIST 0
#define OK 1
#define CANCEL 2

static void parse ();

static void 
checkout_done (Agent * pAgent)
{
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[0]);
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[1]);

  /*FIXME: Optimize by only rebuilding the currently selected
     tree node */
  pharmacy_rebuild_entire_dirtree ();

  g_free (pgcCheckoutBuffer);
  pgcCheckoutBuffer = NULL;

  gnome_appbar_pop (pharmacy_get_appbar ());
}

static void 
checkout_read (Agent * pAgent, gchar * str, gint count)
{
  concat_to_buffer (&pgcCheckoutBuffer, str);

  if (strstr (pgcCheckoutBuffer, LOOK_FOR_ABORT))
    {
      g_free (pgcCheckoutBuffer);
      pgcCheckoutBuffer = NULL;

      gnome_app_message (pharmacy_get_app (),
			 _ ("Checkout failed.  Please look at the console for more information."));
    }
}

static void 
checkout (gchar * str)

{
  gchar *cmd = NULL;
  LocationData *pLoc = NULL;
  Agent *pAgent = pharmacy_get_agent ();
  gchar *pGlobalOptions = NULL;
  gchar *pCheckoutOptions = NULL;

  gdk_flush();

  if (bCheckout &&
      str &&
      str[0] != '0')
    {

      aSignals[0] = gtk_signal_connect (GTK_OBJECT (pAgent),
					"child-died",
					checkout_done,
					NULL);

      aSignals[1] = gtk_signal_connect (GTK_OBJECT (pAgent),
					"read-child-output",
					checkout_read,
					NULL);


      pLoc = (LocationData *) (pharmacy_get_console ()->pCurrentNode->pData);

      g_assert (pLoc);

      pGlobalOptions = cvs_global_get_options ();
      pCheckoutOptions = cvs_checkout_get_options ();

      cmd = g_strconcat ("cvs ", pGlobalOptions, "-d ",
			 locationdata_get_server (pLoc),
			 " checkout ", pCheckoutOptions, str, NULL);

      gnome_appbar_push (pharmacy_get_appbar (),
			 _ ("Performing checkout..."));

      console_execute_cmd (cmd);

      g_free (cmd);
      g_free (pGlobalOptions);
      g_free (pCheckoutOptions);
    }
}

static void 
updatelist_done (Agent * pAgent)

{
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[0]);
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[1]);

  if (pgcCheckoutBuffer)
    {
      concat_to_buffer (&pgcCheckoutBuffer, "\r\n");
      while (pgcCheckoutBuffer && strstr (pgcCheckoutBuffer, "\r\n"))
	parse ();
    }

  g_free (pgcCheckoutBuffer);
  pgcCheckoutBuffer = NULL;

  gnome_appbar_pop (pharmacy_get_appbar ());
}


static void 
parse ()
{
  gchar *pBuffer = pgcCheckoutBuffer;
  gchar *pEOL = NULL;
  gchar *pExtra = NULL;
  gchar *pSpace = NULL;
  gint len = 0;
  gchar *paText[2] =
  {NULL, NULL};


  if (pEOL = strstr (pBuffer, "\r\n"))
    {
      if (!strstr (pBuffer, "About"))
	{
	  pSpace = strstr (pBuffer, " ");
	  if (!pSpace)
	    pSpace = strstr (pBuffer, "\t");
	  if (pSpace)
	    {
	      len = strlen (pBuffer) - strlen (pSpace);
	      pExtra = g_strndup (pBuffer, len);

	      paText[0] = pExtra;

	      if (pExtra && pExtra[0] != 0)
		gtk_clist_append (GTK_CLIST (pModuleList), paText);
	      g_free (pExtra);
	    }
	}
    }
  /* Clean up old part */
  if (strlen (pEOL) == 2)
    {
      g_free (pgcCheckoutBuffer);
      pgcCheckoutBuffer = NULL;
    }
  else
    {
      pExtra = g_strdup (pEOL + 2);
      g_free (pgcCheckoutBuffer);
      pgcCheckoutBuffer = pExtra;
    }
}

static void 
updatelist_read (Agent * pAgent, gchar * str, gint count)

{
  concat_to_buffer (&pgcCheckoutBuffer, str);

  g_debug_message (pgcCheckoutBuffer);

  while (pgcCheckoutBuffer && strstr (pgcCheckoutBuffer, "\r\n"))
    parse ();

}

static void 
updatelist ()
{
  gchar *cmd = NULL;
  LocationData *pLoc = NULL;
  Agent *pAgent = pharmacy_get_console ()->pAgent;

  if (pharmacy_get_console () &&
      pharmacy_get_console ()->pCurrentNode)
    {
      gtk_clist_clear (GTK_CLIST (pModuleList));

      aSignals[0] = gtk_signal_connect (GTK_OBJECT (pAgent),
					"child-died",
					updatelist_done,
					NULL);

      aSignals[1] = gtk_signal_connect (GTK_OBJECT (pAgent),
					"read-child-output",
					updatelist_read,
					NULL);


      pLoc = (LocationData *) (pharmacy_get_console ()->pCurrentNode->pData);

      g_assert (pLoc);

      cmd = g_strconcat ("cvs -z3 -d ",
			 locationdata_get_server (pLoc),
			 " checkout -c", NULL);

      gnome_appbar_push (pharmacy_get_appbar (),
			 _ ("Looking for modules..."));

      console_execute_cmd (cmd);

      g_free (cmd);
      //      g_free(pGlobalOptions);
      //      g_free(pCheckoutOptions);
    }
}

void 
checkout_ui_update_cb (gchar * pgcHint)
{
  CVSConsole *pCon = pharmacy_get_console ();

  if (pCon && pCon->pCurrentNode && pCon->pCurrentNode->pData &&
      !agent_in_use (pharmacy_get_agent ()))
    bCheckout = TRUE;
  else
    bCheckout = FALSE;

  gtk_widget_set_sensitive (repository_menu[MENU].widget,
			    bCheckout);
  gtk_widget_set_sensitive (toolbar[TOOLBAR].widget,
			    bCheckout);
}

static void 
clicked_cb (GnomeDialog * pDialog, gint nButton)
{
  gchar *pModule = NULL;
  
  gdk_flush();

  switch (nButton)
    {
    case UPDATE_LIST:
      updatelist ();
      break;
    case OK:
      pModule = gtk_entry_get_text (GTK_ENTRY (pModuleEntry));
      checkout (pModule);
      g_free (pModule);
    case CANCEL:
      gnome_dialog_close (pDialog);
      break;
    }
}

static void 
select_row_cb (GtkWidget * widget,
	       gint row,
	       gint column,
	       GdkEventButton * event,
	       gpointer data)
{
  gchar *buffer = NULL;

  gtk_clist_get_text (GTK_CLIST (widget),
		      row, 0, &buffer);

  gtk_entry_set_text (GTK_ENTRY (pModuleEntry), buffer);
}

GtkWidget *
checkout_build_ui ()
{
  GnomeDialog *pDialog = GNOME_DIALOG (gnome_dialog_new (
				   _ ("Checkout Module"), _ ("Update List"),
						      GNOME_STOCK_BUTTON_OK,
						  GNOME_STOCK_BUTTON_CANCEL,
							  NULL));
  gnome_dialog_set_parent (pDialog, GTK_WINDOW (pharmacy_get_app ()));


  pModuleEntry = gtk_entry_new ();
  pModuleList = gtk_clist_new (1);

  gtk_box_pack_start (GTK_BOX (pDialog->vbox),
		      pModuleEntry, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (pDialog->vbox),
		      util_contain_in_scrolled (pModuleList), TRUE, TRUE, 0);

  gtk_widget_set_usize (pModuleList, 100, 200);

  gtk_widget_show (pModuleEntry);
  gtk_widget_show (pModuleList);

  gnome_dialog_close_hides (pDialog, TRUE);

  gtk_signal_connect (GTK_OBJECT (pDialog), "clicked",
		      clicked_cb, NULL);
  gtk_signal_connect (GTK_OBJECT (pModuleList), "select_row",
		      select_row_cb, NULL);

  gtk_clist_set_sort_column (GTK_CLIST (pModuleList), 0);

  return GTK_WIDGET (pDialog);
}

void 
checkout_cb ()
{
  GtkWidget *pDlg = NULL;
  g_return_if_fail (bCheckout);

  gdk_flush();

  if (!pCheckoutDialog)
    pCheckoutDialog = checkout_build_ui ();
  gtk_widget_show (pCheckoutDialog);

}


