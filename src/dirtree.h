#ifndef DIR_TREE_H
#define DIR_TREE_H 1

#include "location.h"

   typedef struct
   {
   /* This is first right now because the root items are not of the same type.
     But WorkDir is first in LocationData, so it works. THIS WILL CHANGE */
      gchar* pgcDir;  /* Path with slash at end */
		gchar* pgcName;
      /* Hashing. Check in order */
      gchar nLetters; /* Used to cheat lookups */
      gchar nSlashes; /* Used to cheat lookups */
      guint nHash;    /* Used to cheat lookups */
      
      gchar sig; /* Help verify that this is a real node */
      
      /* Future location of location data */
      gpointer pData;
      
      /* Referance counting */
      guint uiRef;
      gint type;
      gint hasSubdirs;
   } DirTreeNode;

   gint dirtree_fill_tree(GtkCTree* tree,GtkCTreeNode* pList,
   const gchar* str, 
   gint nMax, gint nLevel);

   void dirtree_init(GtkCTree* pTree);

   void dirtree_load_size(GtkWidget* pWidget);
   void dirtree_save_size(GtkWidget* pWidget);

/*gint dirtreenode_cmp(DirTreeNode*,DirTreeNode*);*/
   gpointer dirtreenode_new_root(LocationData* pLoc);
   gpointer dirtreenode_new ();
   void dirtreenode_set_dir (DirTreeNode * pNode, gchar * pgcDir);
   void dirtreenode_free(DirTreeNode* pNode);
   DirTreeNode* dirtreenode_add_ref(DirTreeNode* pNode);
   void dirtree_purge(GtkCTree* ctree, GtkCTreeNode* node);
   void dirtree_set_last_location(LocationData* pData);
#endif /* DIR_TREE_H */
