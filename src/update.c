/* update.c
   This file is part of "Pharmacy: A GNOME CVS front-end"
   Copyright 1998 Reklaw (N. Adam Walker)
   "Pharmacy" is released under the GPL
   See the LICENCE for more details.
 */
#include <config.h>
#include <gnome.h>

#include "pharmacy.h"
#include "console.h"
#include "preferences.h"

static gint aSignals[2];
static gchar *pgcUpdateBuffer = NULL;
static gint bUpdate = FALSE;

#define LOOK_FOR_FAILED "update failed"
#define MENU_UPDATE 3
#define MENU_GET_LATEST 4
#define TOOLBAR_UPDATE 1

void 
update_ui_update_cb ()
{
  CVSConsole *pCon = pharmacy_get_console ();
  GtkCList* pList = GTK_CLIST(pharmacy_get_filelist());

  if (pCon && pCon->pCurrentNode &&
      !agent_in_use (pharmacy_get_agent ()))
    bUpdate = TRUE;
  else
    bUpdate = FALSE;

  gtk_widget_set_sensitive (repository_menu[MENU_UPDATE].widget, bUpdate);
  gtk_widget_set_sensitive (toolbar[TOOLBAR_UPDATE].widget, bUpdate);

  gtk_widget_set_sensitive (repository_menu[MENU_GET_LATEST].widget,
			    bUpdate);
}

static void 
update_done (Agent * pAgent)
{
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[0]);
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[1]);

  g_free (pgcUpdateBuffer);
  pgcUpdateBuffer = NULL;

  filelist_rebuild_list (pharmacy_get_filelist (),
			 pharmacy_get_console ()->pCurrentNode);


  gnome_appbar_pop (pharmacy_get_appbar ());
}

static void 
update_read (Agent * pAgent, gchar * str, gint count)
{
  concat_to_buffer (&pgcUpdateBuffer, str);

  if (strstr (pgcUpdateBuffer, LOOK_FOR_FAILED))
    {
      g_free (pgcUpdateBuffer);
      pgcUpdateBuffer = NULL;

      gnome_app_message (pharmacy_get_app (),
			 _ ("Update failed.  Please look at the console for more information."));
    }
}


static void
update_cvs(gint bRemoveFirst)
{

  Agent *pAgent = pharmacy_get_console ()->pAgent;
  gchar *pCmd = NULL;
  gchar *pGlobalOptions = NULL;
  gchar *pUpdateOptions = NULL;
  gchar *pFiles = NULL;
  gchar* pRm = NULL;

  g_return_if_fail (bUpdate);

  gdk_flush();

  aSignals[0] = gtk_signal_connect (GTK_OBJECT (pAgent),
				    "child-died",
				    update_done,
				    NULL);

  aSignals[1] = gtk_signal_connect (GTK_OBJECT (pAgent),
				    "read-child-output",
				    update_read,
				    NULL);

  gnome_appbar_push (pharmacy_get_appbar (),
		     _ ("Performing update..."));


  pGlobalOptions = cvs_global_get_options ();
  pUpdateOptions = cvs_update_get_options ();

  pFiles = filelist_get_selected_files (pharmacy_get_filelist ());

  if(bRemoveFirst)
    {
      pRm = g_strconcat("rm -f ", pFiles?pFiles:"*"," && ");
    }

  pCmd = g_strconcat (pRm ? pRm : "" , "cvs ", pGlobalOptions,
		      "update ", pUpdateOptions,
		      pFiles ? pFiles : "",
		      NULL);
  console_execute_cmd (pCmd);

  g_free (pGlobalOptions);
  g_free (pUpdateOptions);
  g_free (pCmd);
  g_free (pFiles);
  g_free (pRm);
}

static void
get_latest_reply(gint reply,gpointer data)
{
  if(reply==0)
    {
      update_cvs(TRUE);
    }
}

void
get_latest_version_cb()
{
  gint bAnnoyUser = TRUE;

  // FIXME: Make AnnoyUser an option.
  if(bAnnoyUser)
    {
      gnome_app_question(pharmacy_get_app(),_("This will lose any changes. Are you sure you wish to continue?"),get_latest_reply,NULL);
    }
}

void 
update_cb ()
{
  update_cvs(FALSE);
}

