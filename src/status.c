/* status.c
   This file is part of "Pharmacy: A GNOME CVS front-end"
   Copyright 1998 Reklaw (N. Adam Walker)
   "Pharmacy" is released under the GPL
   See the LICENCE for more details.

   This file contains the functions needed to implement
   The status command.
 */

#include "config.h"
#include <gnome.h>
#include "pharmacy.h"
#include "preferences.h"

#include "filelist.h"
#include "status.h"

static gint aSignals[2];
static gchar *pgcStatusBuffer = NULL;

static gboolean bStatus = FALSE;

#define MENU 0
#define TOOLBAR 0

static void
tab_to_space (gchar * pStr)
{
  gchar *temp;

  while ((temp = strstr (pStr, "\t")))
    (*temp) = ' ';
}

static void
status_done (Agent * pAgent)
{
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[0]);
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[1]);

  g_free (pgcStatusBuffer);
  pgcStatusBuffer = NULL;

  gnome_appbar_pop (pharmacy_get_appbar ());
}

gchar *
get_next_line (gchar ** pBuffer)
{
  gchar *pRet = NULL;
  gchar *pSearch = NULL;
  gchar *pTemp = NULL;
  gint nLen = 0;

  g_return_val_if_fail (pBuffer, NULL);
  g_return_val_if_fail ((*pBuffer), NULL);

  pSearch = strstr ((*pBuffer), "\n");

  if (pSearch)
    {
      nLen = strlen ((*pBuffer)) - strlen (pSearch);

      pRet = g_strndup ((*pBuffer), nLen);


      if (strlen (pSearch + 1))
	pTemp = g_strdup (pSearch + 1);
      else
	pTemp = NULL;
      g_free ((*pBuffer));
      (*pBuffer) = pTemp;
    }

  return pRet;
}


static void
parse (gchar * pgcLine)
{
  gchar *pOutput = NULL;
  gchar *pEnd = NULL;
  gchar *pTemp = NULL;
  gchar *pNoFile = NULL;
  gint nRow = -1;
  FileListNodeStatus eStatus = FLN_STATUS_UNKNOWN;


  g_return_if_fail (pgcLine);

  tab_to_space (pgcLine);

  /* Seperator */
  if ('=' == pgcLine[0])
    {
      /* Flush structure */
    }
  else
    {
      /* if 'File:' at begining of line */
      if ('F' == pgcLine[0] && (pOutput = strstr (pgcLine, "File: ")))
	{
	  /* Get filename */
	  pOutput += 6;
	  pEnd = strstr (pOutput, " ");
	  g_return_if_fail (pEnd);

	  if (strstr (pOutput, "no file "))
	    {
	      pOutput += 8;
	      pEnd = strstr (pOutput, " ");
	      g_return_if_fail (pEnd);
	    }

	  pTemp = g_strndup (pOutput, strlen (pOutput) - strlen (pEnd));
	  nRow = filelist_find_row_with_filename (pharmacy_get_filelist (),
						  pTemp);

	  g_free (pTemp);
	  pTemp = NULL;

	  /* Get status */
	  pOutput = strstr (pEnd, "Status: ");
	  g_return_if_fail (pOutput);
	  pOutput += 8;

	  if (strstr (pOutput, "Up-to-date"))
	    {
	      /* File is up-to-date */
	      eStatus = FLN_STATUS_NORMAL;
	    }
	  else if (strstr (pOutput, "Needs Patch"))
	    {
	      /* File needs a patch */
	      eStatus = FLN_STATUS_NEEDS_PATCH;
	    }
	  else if (strstr (pOutput, "Locally Modified"))
	    {
	      /* Locally Modified */
	      eStatus = FLN_STATUS_MODIFIED;
	    }
	  else if (strstr (pOutput, "Locally Removed"))
	    {
	      /* Locally Removed */
	      eStatus = FLN_STATUS_REMOVED;
	    }
	  else if (strstr (pOutput, "Locally Added"))
	    {
	      /* Locally Added */
	      eStatus = FLN_STATUS_ADDED;
	    }
	  else if (strstr (pOutput, "Needs Checkout"))
	    {
	      /* File does not exist locally, needs a checkout or update */
	      eStatus = FLN_STATUS_NEEDS_CHECKOUT;
	    }
	  else if (strstr (pOutput, "Needs Merge"))
	    {
	      /* Local Modified older version is local. 
	         Newer version on server */
	      eStatus = FLN_STATUS_NEEDS_MERGE;
	    }
	  else
	    {
	      /* Default */
	      pTemp = g_strdup_printf (_ ("Unknown status: \"%s\". Please summit a bug report."), pOutput);
	      gnome_app_message (GNOME_APP (pharmacy_get_app ()),
				 pTemp);
	      g_free (pTemp);
	      pTemp = NULL;
	    }

	  filelist_row_set_status (pharmacy_get_filelist (), nRow, eStatus);

	}

      /* Working Version */
      /* Reposititory Version */
      /* Sticky Tag */
      /* Sticky Date */
      /* Sticky Options */
    }
}

static void
status_read (Agent * pAgent, gchar * str, gint count)
{
  gchar *pgcLine = NULL;
  concat_to_buffer (&pgcStatusBuffer, str);

  while (pgcStatusBuffer &&
	 (pgcLine = get_next_line (&pgcStatusBuffer)))
    {
      parse (pgcLine);
      g_free (pgcLine);
    }
}

void
status_cb ()
{
  gchar *cmd = NULL;
  LocationData *pLoc = NULL;
  Agent *pAgent = pharmacy_get_agent ();
  gchar *pGlobalOptions = NULL;
  gchar *pStatusOptions = NULL;
  gchar *pFiles = NULL;

  gdk_flush ();

  if (bStatus)
    {

      aSignals[0] = gtk_signal_connect (GTK_OBJECT (pAgent),
					"child-died",
					status_done,
					NULL);

      aSignals[1] = gtk_signal_connect (GTK_OBJECT (pAgent),
					"read-child-output",
					status_read,
					NULL);


      pLoc = (LocationData *) (pharmacy_get_console ()->pCurrentNode->pData);

      g_assert (pLoc);

      pGlobalOptions = cvs_global_get_options ();
      pStatusOptions = cvs_status_get_options ();

      pFiles = filelist_get_selected_files (pharmacy_get_filelist ());
      cmd = g_strconcat ("cvs ", pGlobalOptions,
			 "status ", pStatusOptions,
			 pFiles ? pFiles : "",
			 NULL);

      gnome_appbar_push (pharmacy_get_appbar (),
			 _ ("Getting file status..."));

      console_execute_cmd (cmd);

      g_free (cmd);
      g_free (pGlobalOptions);
      g_free (pStatusOptions);
      g_free (pFiles);
    }
  gdk_flush ();
}

static void
status_ui_update_cb (gchar * pgcHint)
{
  CVSConsole *pCon = pharmacy_get_console ();

  if (pCon && pCon->pCurrentNode &&
      !agent_in_use (pharmacy_get_agent ()))
    bStatus = TRUE;
  else
    bStatus = FALSE;

  gtk_widget_set_sensitive (view_menu[MENU].widget, bStatus);
  gtk_widget_set_sensitive (toolbar[TOOLBAR].widget, bStatus);
}

void
status_init ()
{
  /* Connect with ui_update and other groovy things here */
  gtk_signal_connect (GTK_OBJECT (pharmacy_get_app ()),
		      PHARMACY_UI_UPDATE_SIGNAL,
		      status_ui_update_cb, NULL);
}

gchar *
status_to_text (gint eStatus)
{
  gchar *pStr = NULL;

  switch (eStatus)
    {
    case FLN_STATUS_NORMAL:
      pStr = _ ("Up-to-date");
      break;

    case FLN_STATUS_ADDED:
      pStr = _ ("Added");
      break;

    case FLN_STATUS_REMOVED:
      pStr = _ ("Removed");
      break;

    case FLN_STATUS_NEEDS_PATCH:
      pStr = _ ("Needs Patch");
      break;

    case FLN_STATUS_MODIFIED:
      pStr = _ ("Modified");
      break;

    case FLN_STATUS_NEEDS_CHECKOUT:
      pStr = _ ("Needs Checkout");
      break;

    case FLN_STATUS_NEEDS_MERGE:
      pStr = _ ("Needs Merge");
      break;

    case FLN_STATUS_IGNORE:
      pStr = _ ("* ignore *");
      break;

    case FLN_STATUS_NOT_IN_CVS:
      pStr = _ ("* not in cvs *");
      break;

    case FLN_STATUS_UNKNOWN:
    default:
      pStr = _ ("* unknown status *");
    }

  return pStr;
}
