/* add.c
   This file is part of "Pharmacy: A GNOME CVS front-end"
   Copyright 1998 Reklaw (N. Adam Walker)
   "Pharmacy" is released under the GPL
   See the LICENCE for more details.
 */
#include <config.h>
#include <gnome.h>

#include "pharmacy.h"
#include "console.h"
#include "preferences.h"
#include "agent.h"

#include <string.h>

static gint aSignals[2];
static gchar *pgcAddBuffer = NULL;
static gint bAdd = FALSE;
static gboolean success_add = FALSE;

#define MENU 10
#define TOOLBAR 7

static GtkWidget* s_pFileSel = NULL;

void 
add_ui_update_cb (gchar * pgcHint)
{
  CVSConsole *pCon = pharmacy_get_console ();

  if (pCon && pCon->pCurrentNode &&
      !agent_in_use (pharmacy_get_agent ()))
    bAdd = TRUE;
  else
    bAdd = FALSE;

  gtk_widget_set_sensitive (repository_menu[MENU].widget, bAdd);
  gtk_widget_set_sensitive(toolbar[TOOLBAR].widget,bAdd);

}

static void 
add_done (Agent * pAgent)
{
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[0]);
  gtk_signal_disconnect (GTK_OBJECT (pAgent), aSignals[1]);

  if(success_add)
    {
      gnome_app_message(GNOME_APP(pharmacy_get_app()),
			_("File added. Use \'commit\' to upload the file."));
      }
  else
    {
      gnome_app_message(GNOME_APP(pharmacy_get_app()),
			_("The file could not be added to " \
			    "the repository..."));
    }

  g_free (pgcAddBuffer);
  pgcAddBuffer = NULL;

  filelist_rebuild_list (pharmacy_get_filelist (),
			 pharmacy_get_console ()->pCurrentNode);

  gnome_appbar_pop (pharmacy_get_appbar ());
}

static void
parse(gchar* line)
{
  gchar* end;

  end = strstr(line, "cvs commit");
  if(end)
    {
      success_add = TRUE;
    }
}

static void 
add_read (Agent * pAgent, gchar * str, gint count)
{
  gchar* line = NULL;

  concat_to_buffer (&pgcAddBuffer, str);

  while (pgcAddBuffer &&
	 (line = get_next_line (&pgcAddBuffer)))
    {
      parse (line);
      g_free (line);
    }

}

static void
open_canceled_cb ()
{
  gtk_widget_destroy (GTK_WIDGET (s_pFileSel));
  s_pFileSel = NULL;
}

static void
open_fileselected_cb ()
{
  /* FIXME: Do I free this or what? */
  // gchar *pFilename = NULL;
  //FILE* pFile = NULL;
  //  gint nRead = 0;
  //DiffControl* pCntl = NULL;
  //gchar *pgcLine = NULL;
  //gchar str[DC_BUFF_LEN];

  Agent *pAgent = pharmacy_get_console ()->pAgent;
  gchar *pCmd = NULL;
  gchar *pGlobalOptions = NULL;
  gchar *pFiles = NULL;
  gchar *pDir = NULL;
  gchar *temp = NULL;


  g_return_if_fail (s_pFileSel);

  temp = gtk_file_selection_get_filename (GTK_FILE_SELECTION (s_pFileSel));
  temp = strrchr(temp,'/');

  if(temp == NULL)
    return;

  pFiles = g_strdup(temp+1);
  

  open_canceled_cb ();

  g_debug_message (pFiles);

  /* Meat */
  aSignals[0] = gtk_signal_connect (GTK_OBJECT (pAgent),
				    "child-died",
				    add_done,
				    NULL);

  aSignals[1] = gtk_signal_connect (GTK_OBJECT (pAgent),
				    "read-child-output",
				    add_read,
				    NULL);

  gnome_appbar_push (pharmacy_get_appbar (),
		     _ ("Adding file..."));

  pDir = g_strdup (pharmacy_get_console ()->pCurrentNode->pgcDir);

  pGlobalOptions = cvs_global_get_options ();

  // pFiles = filelist_get_selected_files (pharmacy_get_filelist ());
  pCmd = g_strconcat ("cvs ", pGlobalOptions,
		      "add ",
		      pFiles ? pFiles : "",
		      NULL);
  console_execute_cmd_in_dir (pCmd, pDir);

  g_free (pGlobalOptions);
  g_free (pCmd);
  g_free (pFiles);
  g_free (pDir);



}

void 
add_cb ()
{
  gchar * pDir = NULL;
  g_return_if_fail (bAdd);

  pDir = g_strdup (pharmacy_get_console ()->pCurrentNode->pgcDir);
  success_add = FALSE;

  if (s_pFileSel == NULL)
    {
      s_pFileSel = gtk_file_selection_new (_ ("Add a file to CVS"));
    }

  gtk_file_selection_set_filename(GTK_FILE_SELECTION(s_pFileSel),pDir);
  gtk_widget_show (s_pFileSel);

  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (s_pFileSel)->ok_button),
		      "clicked",
		      open_fileselected_cb, NULL);

  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (s_pFileSel)->
				  cancel_button),
		      "clicked",
		      open_canceled_cb, NULL);

  g_free(pDir);

}







